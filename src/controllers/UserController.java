package controllers;

import java.lang.reflect.Type;

import dto.UserDTO;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import app.IO;
import dto.CurrentUserDTO;
import dto.LoginDTO;
import managers.UserManager;
import spark.Route;
import spark.Session;
import users.Administrator;
import users.Buyer;
import users.Role;
import users.Seller;
import users.User;

public class UserController {
	private static Gson gson = new Gson();
	private static Gson gsonDate = new GsonBuilder().registerTypeAdapter(LocalDate.class, new JsonDeserializer<LocalDate>() { 
		@Override 
		public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException { 
			return LocalDate.parse(json.getAsString(), DateTimeFormatter.ofPattern("yyyy-MM-dd")); 
		}}).create();

	public static Route getUsers = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can see registered users!";
		}
		List<UserDTO> dtos = new ArrayList<UserDTO>();
		
		for(User u: UserManager.getInstance().getActiveUsers()) {
			UserDTO dto = new UserDTO(u, null, null);
			
			if(u.getRole().equals(Role.BUYER)) {
				Buyer currentBuyer = UserManager.getInstance().getBuyerByUsername(u.getUsername());
				dto.setBuyerType(currentBuyer.getBuyerType());
				dto.setPoints(currentBuyer.getPoints());
			}
			dtos.add(dto);
		}
		System.out.println("USERI NAD USERIMA:" + gson.toJson(dtos));
		return gson.toJson(dtos);
	};
	
	
	public static Route search = (req, res) -> {
		res.type("application/json");
		
		String firstName = req.queryParams("firstName");
		String lastName = req.queryParams("lastName");
		String username = req.queryParams("username");
		
		return gson.toJson(UserManager.getInstance().search(firstName, lastName, username));
		
	};
	
	public static Route getAdministrators = (req, res) -> {
		res.type("application/json");
		
		return gson.toJson(UserManager.getInstance().getActiveAdministrators());
	};
	
	public static Route getBuyersBySellerUsername = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.SELLER) {
			res.status(403);
			return "Only sellers can see thir buyers!";
		}
		
		String username = req.queryParams("username");
		List<Buyer> buyers = UserManager.getInstance().getBuyersBySellerUsername(username);
		
		buyers = buyers.stream().filter(b -> !b.getIsDeleted()).collect(Collectors.toList());
		
		return gson.toJson(buyers);
	};
	
	public static Route getBuyers = (req, res) -> {
		res.type("application/json");
		
		return gson.toJson(UserManager.getInstance().getActiveBuyers());
	};
	
	public static Route getSellers = (req, res) -> {
		res.type("application/json");
		
		return gson.toJson(UserManager.getInstance().getActiveSellers());
	};
	
	public static Route addSeller = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can add sellers!";
		}
		
		Seller seller;
		try {
			seller = gsonDate.fromJson(req.body(), Seller.class);
		} catch (Exception e) {
			res.status(400);
			return "Bad request! You haven't entered data correctly.";
		}
	
		if (UserManager.getInstance().getUserByUsername(seller.getUsername()) != null) {
			res.status(400);
			return "Username must be unique!";
		}
		
		UserManager.getInstance().addSeller(seller);
		
		return gson.toJson(seller);
	};
	
	public static Route deleteUser = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can delete users!";
		}
		
		String username = req.queryParams("username");	
		User user = UserManager.getInstance().getUserByUsername(username);
		if (user == null || user.getRole() == Role.ADMINISTRATOR) {
			res.status(400);
			return "User " + username + " can't be deleted!";
		}
		
		UserManager.getInstance().deleteUser(user);
		
		if (user.getRole() == Role.BUYER) {
			IO.saveBuyers(UserManager.getInstance().getBuyers());
		} else {
			IO.saveSellers(UserManager.getInstance().getSellers());
		}
		
		return gson.toJson(UserManager.getInstance().getActiveUsers());
	};
	
	public static Route getManifestationsBySeller = (req, res) -> {
		res.type("application/json");

		String username = req.session().attribute("username");
		if (req.session().attribute("role") != Role.SELLER || !username.equals(req.session().attribute("username"))) {
			res.status(403);
			return "Only sellers can see their manifestations!";
		}
		
		return gson.toJson(UserManager.getInstance().getManifestationsBySeller(username));
	};
	
	public static Route verifyLogin = (req, res) -> {
		res.type("application/json");
		
		LoginDTO dto;
		try {
			dto = gson.fromJson(req.body(), LoginDTO.class);
		}catch(Exception e) {
			res.status(400);
			return "Bad request";
			
		}
		
		if(UserManager.getInstance().tryLogin(dto)) {
			User user = UserManager.getInstance().getUserByUsername(dto.getUsername());
			req.session(true).attribute("role", user.getRole());
			req.session(false).attribute("username", user.getUsername());
			
			return "OK";
		}else {
			res.status(403);
			return "Invalid username or password!";
		}
	};
	
	public static Route getUserInfo = (req, res) -> {
		res.type("application/json");
		Session session = req.session(false);
		CurrentUserDTO currentUser = new CurrentUserDTO();
		
		if(session == null) {
			return gson.toJson(currentUser);
		}
		if(session.attribute("role") == null) {
			return gson.toJson(currentUser);
		}
		
		currentUser.isLoggedIn = true;
		currentUser.role = session.attribute("role").toString();
		currentUser.username = session.attribute("username");
		
		return gson.toJson(currentUser);
	};
	
	public static Route logout = (req, res) -> {
		res.type("application/json");
		
		req.session(false).invalidate();
		return gson.toJson("You've logged out successfully.");
	};
	
	public static Route registerBuyer = (req, res) -> {
		res.type("application/json");
		System.out.println("KUPCI");
		System.out.println(UserManager.getInstance().getBuyers());
		
		Buyer buyer;
		try {
			buyer = gsonDate.fromJson(req.body(), Buyer.class);
		} catch(Exception e) {
			res.status(400);
			return "Bad request";
		}
		
		if (UserManager.getInstance().getUserByUsername(buyer.getUsername()) != null) {
			res.status(400);
			return "Username must be unique!";
		}

		UserManager.getInstance().addBuyer(buyer);
		
		System.out.println("KUPCI POSLE");
		System.out.println(UserManager.getInstance().getBuyers());

		return gson.toJson(buyer);
	};
	
	public static Route getUser = (req, res) -> {
		res.type("application/json");
		
		String username = req.session().attribute("username");

		return gson.toJson(UserManager.getInstance().getUserByUsername(username));
	};
	
	public static Route editUser = (req, res) -> {
		res.type("application/json");
		
		Role role = req.session().attribute("role");
		
		if (role == Role.ADMINISTRATOR) {
			Administrator administrator;
			try {
				administrator = gsonDate.fromJson(req.body(), Administrator.class);
			} catch (Exception e) {
				res.status(400);
				return "Bad request! You haven't entered data correctly.";
			}
			
			UserManager.getInstance().editAdministrator(administrator);
			IO.saveAdministrators(UserManager.getInstance().getAdministrators());
			
			return gson.toJson(administrator);
		} else if (role == Role.BUYER) {
			Buyer buyer;
			try {
				buyer = gsonDate.fromJson(req.body(), Buyer.class);
			} catch (Exception e) {
				res.status(400);
				return "Bad request! You haven't entered data correctly.";
			}
			
			UserManager.getInstance().editBuyer(buyer);
			IO.saveBuyers(UserManager.getInstance().getBuyers());
			
			return gson.toJson(buyer);
		} else {
			Seller seller;
			try {
				seller = gsonDate.fromJson(req.body(), Seller.class);
			} catch (Exception e) {
				res.status(400);
				return "Bad request! You haven't entered data correctly.";
			}
			
			UserManager.getInstance().editSeller(seller);
			IO.saveSellers(UserManager.getInstance().getSellers());
			
			return gson.toJson(seller);
		}
	};
}
