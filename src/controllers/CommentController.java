package controllers;

import com.google.gson.Gson;

import app.IO;
import entities.Comment;
import managers.CommentManager;
import spark.Route;
import users.Role;

public class CommentController {
	private static Gson gson = new Gson();
	
	public static Route getComments = (req, res) -> {
		res.type("application/json");
		
		return gson.toJson(CommentManager.getInstance().getComments());
	};
	
	public static Route getCommentsByManifestation = (req, res) -> {
		res.type("application/json");
		
		Integer manifestationId = Integer.parseInt(req.queryParams("id"));
		
		return gson.toJson(CommentManager.getInstance().getCommentsByManifestation(manifestationId));
	};
	
	public static Route deleteComment = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can delete comments!";
		}
		
		Comment comment;
		try {
			comment = gson.fromJson(req.body(), Comment.class);
		} catch (Exception e) {
			res.status(400);
			return "Bad request!";
		}
		
		CommentManager.getInstance().deleteComment(comment);
		IO.saveComments(CommentManager.getInstance().getAllComments());
		
		return gson.toJson(CommentManager.getInstance().getCommentsByManifestation(comment.getManifestation()));
	};
	
	public static Route getCreatedComments = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can see created comments!";
		}
		
		return gson.toJson(CommentManager.getInstance().getCreatedComments());
	};
	
	public static Route approveRejectComment = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can approve or reject comments!";
		}
		
		Comment comment;
		try {
			comment = gson.fromJson(req.body(), Comment.class);
		} catch (Exception e) {
			res.status(400);
			return "Bad request!";
		}
		
		Boolean approve = Boolean.parseBoolean(req.queryParams("approve"));
		if (approve) {
			CommentManager.getInstance().approveComment(comment);
		} else {
			CommentManager.getInstance().rejectComment(comment);
		}
		
		IO.saveComments(CommentManager.getInstance().getAllComments());
		
		return gson.toJson(CommentManager.getInstance().getCreatedComments());
	};
}
