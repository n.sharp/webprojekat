package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;

import app.IO;
import entities.Ticket;
import managers.TicketManager;
import managers.UserManager;
import spark.Route;
import users.Buyer;
import users.BuyerType;
import users.Role;

public class TicketController {
	private static Gson gson = new Gson();
	
	public static Route getTickets = (req, res) -> {
		res.type("application/json");
		
		Role role = req.session().attribute("role"); 
		String username = req.session().attribute("username");
		
		if (role == Role.ADMINISTRATOR || role == Role.SELLER || role == Role.BUYER) {
			return gson.toJson(TicketManager.getInstance().getTicketsDTO(role, username));
		} else {
			res.status(403);
			return "Not registered users can't access ticket information!";
		}
	};
	
	public static Route searchTickets = (req, res) -> {
		res.type("application/json");
		
		String username = req.session().attribute("username");
		String manifestation = req.queryParams("manifestation");
		Double priceFrom = -1.0;
		if (!req.queryParams("priceFrom").equals("")) {
			priceFrom = Double.parseDouble(req.queryParams("priceFrom"));
		}
		Double priceTo = -1.0;
		if (!req.queryParams("priceTo").equals("")) {
			priceTo = Double.parseDouble(req.queryParams("priceTo"));
		}
		String dateFrom = req.queryParams("dateFrom");
		String dateTo = req.queryParams("dateTo");
		
		return gson.toJson(TicketManager.getInstance().searchTickets(username, manifestation, priceFrom, priceTo, dateFrom, dateTo));
	};
	
	public static Route deleteTicket = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can delete tickets!";
		}
		
		String id = req.queryParams("id");
		Ticket ticket = TicketManager.getInstance().getTicketById(id);
		if (ticket == null) {
			res.status(400);
			return "Ticket with id " + id + " doesn't exist!";
		}
		
		TicketManager.getInstance().deleteTicket(ticket);
		IO.saveTickets(TicketManager.getInstance().getAllTickets());
		
		return gson.toJson(TicketManager.getInstance().getTicketsDTO(Role.ADMINISTRATOR, ""));
	};
	
	public static Route calculatePriceNew = (req, res) -> {
		res.type("application/json");
		
		if(req.session().attribute("role") != Role.BUYER) {
			res.status(403);
			return "Only buyers can calculate price!";
		}
		
		String username =req.queryParams("username");
		Double basePrice = Double.parseDouble(req.queryParams("basePrice"));
		Integer numOdVIP = Integer.parseInt(req.queryParams("numOfVIP"));
		Integer numOfFanpit = Integer.parseInt(req.queryParams("numOfFanpit"));
		Integer numOfRegular = Integer.parseInt(req.queryParams("numOfRegular"));
		
		Buyer b = UserManager.getInstance().getBuyerByUsername(username);
		BuyerType type = b.getBuyerType();
		
		Double price = TicketManager.getInstance().calculatePriceNew(type,  basePrice, numOdVIP ,numOfFanpit, numOfRegular);
		
		return new JsonPrimitive(price);
	};
	
	public static Route buyTickets = (req, res) -> {
		res.type("application/json");
		
		if(req.session().attribute("role") != Role.BUYER) {
			res.status(403);
			return "Only buyers can buy tickets!";
		}
		Integer manId = Integer.parseInt(req.queryParams("manId"));
		String username = req.queryParams("username");
		Integer numVIP = Integer.parseInt(req.queryParams("numVIP"));
		Integer numFanpit = Integer.parseInt(req.queryParams("numFanpit"));
		Integer numReg = Integer.parseInt(req.queryParams("numReg"));
		
		Boolean success = TicketManager.getInstance().buyTickets(manId, username, numVIP, numFanpit, numReg); 
		if(!success) {
			res.status(404);
			return "Not enough available tickets";
		}
		return "OK";
	};
}
