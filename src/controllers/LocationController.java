package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonPrimitive;

import app.IO;
import entities.Location;
import entities.Manifestation;
import managers.LocationManager;
import managers.ManifestationManager;
import spark.Route;
import users.Role;

public class LocationController {
	private static Gson gson = new Gson();
	
	public static Route getLocations = (req, res) -> {
		res.type("application/json");
		
		return gson.toJson(LocationManager.getInstance().getLocations());
	};
	
	public static Route getLocationById = (req, res) -> {
		res.type("application/json");
		
		LocationManager lm = LocationManager.getInstance();
		Location location =lm.getLocationById(Integer.parseInt(req.params(":id")));
		
		if (location == null) {
			res.status(404);
			return "Location does not exist. ID: " + req.params(":id");
		} else {
			return gson.toJson(location);
		}
	};
	
	public static Route deleteLocation = (req, res) -> {
		res.type("application/json");
		
		Integer id = Integer.parseInt(req.queryParams("id"));
		Location location = LocationManager.getInstance().getLocationById(id);
		
		if (location == null) {
			res.status(404);
			return "Location with id " + id + " doesn't exist!";
		} 
		
		for (Manifestation manifestation : ManifestationManager.getInstance().getManifestationsByLocation(id)) {
			ManifestationManager.getInstance().deleteManifestation(manifestation);
		}
		
		LocationManager.getInstance().deleteLocation(location);
		IO.saveLocations(LocationManager.getInstance().getAllLocations());

		return gson.toJson(LocationManager.getInstance().getLocations());
	};
	
	public static Route getIdOfLocationOrCreate = (req, res) -> {
		res.type("application/json");
		
		if(req.session().attribute("role") != Role.SELLER) {
			res.status(403);
			return "Only sellers can add locations!";
		}
		
		Location location;
		try {
			location = gson.fromJson(req.body(), Location.class);
			for(Location l : LocationManager.getInstance().getLocations()) {
				if(l.getLongitude().equals(location.getLongitude())
						&& l.getLatitude().equals(location.getLatitude()) 
						&& !l.getIsDeleted()) {
					return new JsonPrimitive(l.getId());
				}
			} 
			
			//kreiranje nove
			Location newLocation = new Location(generateLocId(), location.getLongitude(), location.getLatitude(), location.getStreet(), location.getCity(), location.getPostalNumber(),
		location.getIsDeleted());
			
			LocationManager.getInstance().addLocation(newLocation);
			
			return new JsonPrimitive(newLocation.getId());
			
		}catch(Exception e) {
			res.status(400);
			return "Bad request!";
		}
		
		
		
		
	};
	
	private static Integer generateLocId() {
		int b = (int)(Math.random()*(500-5+5)+5);  
		return b;
	};
}
