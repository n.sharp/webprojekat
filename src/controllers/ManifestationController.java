package controllers;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import app.IO;
import dto.ManifestationDTO;
import entities.Location;
import entities.Manifestation;
import managers.LocationManager;
import managers.ManifestationManager;
import managers.UserManager;
import spark.Route;
import users.Role;

public class ManifestationController {
	
	private static JsonDeserializer<LocalDateTime> deser =  new JsonDeserializer<LocalDateTime>() { 
		@Override 
		public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException { 
			return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")); 
		}};
		
	private static JsonSerializer<LocalDateTime> ser = new JsonSerializer<LocalDateTime>() {
		@Override
		public JsonElement serialize(LocalDateTime src, Type typeOfSrc, JsonSerializationContext context) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm");
			return new JsonPrimitive(src.format(formatter));
		}};
		
	private static Gson gsonDateTime = new GsonBuilder()
			.registerTypeAdapter(LocalDateTime.class, ser)
			.registerTypeAdapter(LocalDateTime.class, deser)
			.create();

	
	public static Route getManifestations = (req, res) -> {
		res.type("application/json");
		
		List<ManifestationDTO> dtos = ManifestationManager.getInstance().getManifestations()
				.stream()
				.map(manifestation -> new ManifestationDTO(manifestation))
				.collect(Collectors.toList());
		
		return gsonDateTime.toJson(dtos);
	};
	
	public static Route getManifestationsBySeller = (req, res) -> {
		res.type("application/json");
		
		String username = req.session().attribute("username");
		List<ManifestationDTO> dtos = UserManager.getInstance().getManifestationsBySeller(username)
				.stream()
				.map(manifestation -> new ManifestationDTO(manifestation))
				.collect(Collectors.toList());
		
		return gsonDateTime.toJson(dtos);
	};
		
	public static Route getInactiveManifestations = (req, res) -> {
		res.type("application/json");
		
		if(req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only admins can see inactive manifestations!";
		}
		
		List<ManifestationDTO> dtos = ManifestationManager.getInstance().getManifestations()
				.stream()
				.filter(m -> !m.getIsActive() && !m.getIsDeleted())
				.map(manifestation -> new ManifestationDTO(manifestation))
				.collect(Collectors.toList());
		System.out.println("INACTIVE ONES:" + gsonDateTime.toJson(dtos));
		return gsonDateTime.toJson(dtos);
	};
	
	public static Route getActiveManifestations = (req, res) -> {
		res.type("application/json");
		
		
		List<ManifestationDTO> dtos = ManifestationManager.getInstance().getManifestations()
				.stream()
				.filter(m -> m.getIsActive() && !m.getIsDeleted())
				.map(manifestation -> new ManifestationDTO(manifestation))
				.collect(Collectors.toList());
		System.out.println("ACTIVE ONES:" + gsonDateTime.toJson(dtos));
		return gsonDateTime.toJson(dtos);
	};
	
	
	public static Route getManifestationById = (req, res) -> {
		res.type("application/json");
		
		Manifestation manifestation = ManifestationManager.getInstance().getManifestationById(Integer.parseInt(req.params(":id")));
		
		if (manifestation == null) {
			res.status(404);
			return "There is no manifestation with id " + req.params(":id");
		} else {
			return gsonDateTime.toJson(manifestation);
		}
	};
	
	public static Route getManifestationDTOById = (req, res) -> {
		res.type("application/json");
		
		Manifestation manifestation = ManifestationManager.getInstance().getManifestationById(Integer.parseInt(req.params(":id")));
		
		if (manifestation == null) {
			res.status(404);
			return "There is no manifestation with id " + req.params(":id");
		} else {
			ManifestationDTO dto = new ManifestationDTO(manifestation);
			return gsonDateTime.toJson(dto);
		}
	};
	
	public static Route searchManifestations = (req, res) -> {
		res.type("application/json");
		
		String name = req.queryParams("name");
		String dateFrom = req.queryParams("dateFrom");
		String dateTo = req.queryParams("dateTo");
		String location = req.queryParams("location");
		Double priceFrom = -1.0;
		if (!req.queryParams("priceFrom").equals("")) {
			priceFrom = Double.parseDouble(req.queryParams("priceFrom"));
		}
		Double priceTo = -1.0;
		if (!req.queryParams("priceTo").equals("")) {
			priceTo = Double.parseDouble(req.queryParams("priceTo"));
		}
		
		return gsonDateTime.toJson(ManifestationManager.getInstance().searchManfestations(name, dateFrom, dateTo, location, priceFrom, priceTo));
	};

	public static Route createManifestation = (req, res) -> {
		res.type("application/json");
		
		if(req.session().attribute("role") != Role.SELLER) {
			res.status(403);
			return "Only sellers can create manifestations!";
		}
		
		Manifestation newM;
		try{
			newM = gsonDateTime.fromJson(req.body(), Manifestation.class);
			//da li postoji neka isto vreme ista lokacija?
			for (Manifestation m : ManifestationManager.getInstance().getManifestations()) {
				
				Location newLoc = LocationManager.getInstance().getLocationById(newM.getLocationId());
				Location mLoc = LocationManager.getInstance().getLocationById(m.getLocationId());
				
				if (m.getDateTime().equals(newM.getDateTime())
						&& mLoc.getLongitude().equals(newLoc.getLongitude())
						&& mLoc.getLatitude().equals(newLoc.getLatitude())
						&& !m.getIsDeleted()
						) {
					res.status(403);
					return "An event with the same date, time and location exists!";
					
				}
			}
			//nema, kreiraj
			newM.setId(generateManId());
			newM.setIsActive(false);
			newM.setIsDeleted(false);
			
			ManifestationManager.getInstance().addManifestation(newM);
			UserManager.getInstance().addSellersManifestation(req.session().attribute("username"), newM.getId());
			
			return gsonDateTime.toJson(newM);
		}catch(Exception e) {
			res.status(400);
			return "Bad request! You haven't entered data correctly.";
		}
	};
	
	private static Integer generateManId() {
		int b = (int)(Math.random()*(500-5+5)+5);  
		return b;
	}
	
	public static Route approveManifestation = (req, res) -> {
		res.type("application/json");
		
		if(req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only admins can approve manifestations!";
		}
		ManifestationManager mm = ManifestationManager.getInstance();
		
		Manifestation manifestation = mm.getManifestationById(Integer.parseInt(req.queryParams("id")));
		if(manifestation == null || manifestation.getIsDeleted()) {
			res.status(404);
			return "Manifestation with selected id does not exist!";
		}
		
		Integer index = mm.getManifestations().indexOf(manifestation);
		mm.getManifestations().get(index).setIsActive(true);
		IO.saveManifestations(mm.getManifestations());
		
		List<ManifestationDTO> dtos = mm.getManifestations()
				.stream()
				.filter(m -> !m.getIsActive() && !m.getIsDeleted())
				.map(m -> new ManifestationDTO(m))
				.collect(Collectors.toList());
		System.out.println("INACTIVE ONES:" + gsonDateTime.toJson(dtos));
		return gsonDateTime.toJson(dtos);
	};
	
	public static Route deleteManifestation = (req, res) -> {
		res.type("application/json");
		
		if (req.session().attribute("role") != Role.ADMINISTRATOR) {
			res.status(403);
			return "Only administrators can delete manifestations!";
		}
		
		Integer id = Integer.parseInt(req.queryParams("id"));
		Manifestation manifestation = ManifestationManager.getInstance().getManifestationById(id);
		if (manifestation == null) {
			res.status(400);
			return "Manifestation with id " + id + " doesn't exist!";
		}
		
		ManifestationManager.getInstance().deleteManifestation(manifestation);
		
		List<ManifestationDTO> dtos = ManifestationManager.getInstance().getManifestations()
				.stream()
				.map(m -> new ManifestationDTO(m))
				.collect(Collectors.toList());
		
		return gsonDateTime.toJson(dtos);
	};

	public static Route editManifestation = (req, res) -> {
		res.type("application/json");
		
		Role role = req.session().attribute("role");
		if (role != Role.SELLER) {
			res.status(403);
			return "Only sellers can edit their manifestations!";
		}
		
		Manifestation edited = gsonDateTime.fromJson(req.body(), Manifestation.class);;
		try {
			edited = gsonDateTime.fromJson(req.body(), Manifestation.class);
		} catch (Exception e) {
			res.status(400);
			return "Bad request! You haven't entered data correctly.";
		}
		
		for (Manifestation m : ManifestationManager.getInstance().getManifestations()) {
			Location editedLocation = LocationManager.getInstance().getLocationById(edited.getLocationId());
			Location mLocation = LocationManager.getInstance().getLocationById(m.getLocationId());
			
			if (m.getDateTime().equals(edited.getDateTime()) && 
				mLocation.getLongitude().equals(editedLocation.getLongitude()) && 
				mLocation.getLatitude().equals(editedLocation.getLatitude())) {
				res.status(403);
				return "An event with the same date, time and location exists!";
			}
		}
		
		ManifestationManager.getInstance().editManifestation(edited);
		IO.saveManifestations(ManifestationManager.getInstance().getAllManifestations());
		
		return gsonDateTime.toJson(edited);
	};
}

