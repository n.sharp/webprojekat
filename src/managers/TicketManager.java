package managers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

import app.IO;
import dto.TicketDTO;
import entities.Manifestation;
import entities.Ticket;
import entities.TicketStatus;
import entities.TicketType;
import users.BuyerType;
import users.Role;

public class TicketManager {
	private ArrayList<Ticket> tickets;
	
	private static TicketManager instance;
	
	private TicketManager() {
		this.tickets = IO.loadTickets();
	}
	
	public static TicketManager getInstance() {
		if(instance == null) {
			instance = new TicketManager();
		}
		return instance;
	}
	
	public ArrayList<TicketDTO> getTicketsDTO(Role role, String username) {
		ArrayList<TicketDTO> dto = new ArrayList<TicketDTO>();
		Manifestation manifestation;
		
		if (role == Role.ADMINISTRATOR) {
			for (Ticket ticket : tickets) {
				if (!ticket.getIsDeleted()) {
					manifestation = ManifestationManager.getInstance().getManifestationById(ticket.getManifestation());
					dto.add(new TicketDTO(ticket, manifestation.getName(), manifestation.getDateTime()));
				}
			}
		} else if (role == Role.SELLER) {
			for (Ticket ticket : tickets) {
				if (!ticket.getIsDeleted() && ticket.getTicketStatus() == TicketStatus.RESERVED) {
					manifestation = ManifestationManager.getInstance().getManifestationById(ticket.getManifestation());
					dto.add(new TicketDTO(ticket, manifestation.getName(), manifestation.getDateTime()));
				}
			}
		} else {
			for (Ticket ticket : tickets) {
				if (!ticket.getIsDeleted() && ticket.getTicketStatus() == TicketStatus.RESERVED && ticket.getBuyer().equals(username)) {
					manifestation = ManifestationManager.getInstance().getManifestationById(ticket.getManifestation());
					dto.add(new TicketDTO(ticket, manifestation.getName(), manifestation.getDateTime()));
				}
			}
		}
		
		return dto;
	}
	
	public ArrayList<TicketDTO> searchTickets(String username, String manifestation, Double priceFrom, Double priceTo, String dateFrom, String dateTo){
		ArrayList<TicketDTO> buyersTickets = getTicketsDTO(Role.BUYER, username);
		ArrayList<TicketDTO> found = new ArrayList<TicketDTO>();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		for (TicketDTO ticket : buyersTickets) {
			if ((manifestation.equals("") || ticket.getManifestation().toLowerCase().contains(manifestation.toLowerCase())) &&
				(priceFrom == -1 || ticket.getPrice() >= priceFrom) && 
				(priceTo == -1 || ticket.getPrice() <= priceTo) &&
				(dateFrom.equals("") || LocalDate.parse(dateFrom, dtf).atStartOfDay().isBefore(ticket.getDateTime())) &&
				(dateTo.equals("") || LocalDate.parse(dateTo, dtf).atStartOfDay().isBefore(ticket.getDateTime()))) {
				found.add(ticket);
			}
		}
		
		return found;
	}
	
	private String generateId() {
		Random random = new Random();
		char[] word = new char[10]; // words of length 3 through 10. (1 and 2 letter words are boring.)
        for(int j = 0; j < word.length; j++)
        {
            word[j] = (char)('a' + random.nextInt(26));
        }
        return new String(word);
	}
	
	public Double calculatePriceNew(BuyerType type, Double basePrice, Integer numOfVIP, Integer numOfFanpit, Integer numOfRegular) {
		Double total = 0.0;
		for(int i = 0; i< numOfVIP; i++) {
			total += 4 * basePrice;
		}
		
		for(int i = 0; i< numOfFanpit; i++) {
			total += 2* basePrice;
		}
		
		for(int i = 0; i< numOfRegular; i++) {
			total += basePrice;
		}

		Integer discount = 0;
		switch(type) {
			case GOLD:
				discount = 15;
				break;
			case SILVER:
				discount = 10;
				break;
			case BRONZE:
				discount = 5;
				break;
			default:
				discount = 0;
				break;
		}
		total = total - (double)discount/100 * total;
		
		return total;
	}
	public Double calculatePrice(Integer manifestationId, Integer count, TicketType ticketType, BuyerType buyerType) {
		Double resultingPrice = 0.0;
		Manifestation manifestation = ManifestationManager.getInstance().getManifestationById(manifestationId);
		Double basePrice = manifestation.getRegularTicketPrice();
		
		if(ticketType.equals(TicketType.FANPIT)) {
			basePrice = 2 * basePrice;
		}else if(ticketType.equals(TicketType.VIP)) {
			basePrice = 4 * basePrice;
		}
		
		resultingPrice = count * basePrice;
		
		Integer discount = 0;
		switch(buyerType) {
			case GOLD:
				discount = 15;
				break;
			case SILVER:
				discount = 10;
				break;
			case BRONZE:
				discount = 5;
				break;
			default:
				discount = 0;
				break;
		}
		resultingPrice = resultingPrice - (double)discount/100 * resultingPrice;
		
		return resultingPrice;
	}
	
	public Boolean buyTickets(Integer manId, String username, Integer numOfVIP, Integer numOfFanpit, Integer numOfRegular ) {
		Manifestation m = ManifestationManager.getInstance().getManifestationById(manId);
		Integer numOfTickets = numOfVIP + numOfFanpit + numOfRegular;
		if(!m.getIsActive() || getNumOfLeftoverTickets(manId) < numOfTickets) {
			return false;
		}else {
			Double basePrice = m.getRegularTicketPrice();
			
			for(int i = 0; i< numOfVIP; i++) {
				Ticket newTicket = new Ticket(generateId(), manId, 2 * basePrice, username, TicketStatus.RESERVED, entities.TicketType.VIP, false);
				addTicket(newTicket);
				Integer points = (int) ((4 * basePrice)/1000 * 133);
				UserManager.getInstance().addPointsToBuyer(username, points);
			}
			
			for(int i = 0; i< numOfFanpit; i++) {
				Ticket newTicket = new Ticket(generateId(), manId,  2 * basePrice, username, TicketStatus.RESERVED, entities.TicketType.FANPIT, false);
				addTicket(newTicket);
				Integer points = (int) ((4 * basePrice)/1000 * 133);
				UserManager.getInstance().addPointsToBuyer(username, points);
			}
			
			for(int i = 0; i< numOfRegular; i++) {
				Ticket newTicket = new Ticket(generateId(), manId,  basePrice, username, TicketStatus.RESERVED, entities.TicketType.REGULAR, false);
				addTicket(newTicket);
				Integer points = (int) ((basePrice)/1000 * 133);
				UserManager.getInstance().addPointsToBuyer(username, points);
			}
			return true;

		}
	}

	

//	
//	public ArrayList<Ticket> karteKupca(String kid){
//		ArrayList<Ticket> vrati = new ArrayList<Ticket>();
//		for (Ticket k : tickets) {
//			if( !k.getKupacId().equals(kid) || k.isObrisano() 
//					|| !k.getStatus().equals(TicketStatus.REZERVISANO)) {
//				continue;
//			}
//			vrati.add(k);
//		}
//		return vrati;
//	}

	public ArrayList<Ticket> getReservedTicketsBySeller(String username) {
		List<Ticket> resultTickets;
		ArrayList<Integer> manifestations = UserManager.getInstance().getManifestationsIdsBySeller(username);
		
		resultTickets = tickets.stream().filter(t -> 
				t.getTicketStatus().equals(TicketStatus.RESERVED) && 
				!t.getIsDeleted() &&
				manifestations.contains(t.getManifestation()))
				.collect(Collectors.toList());
		
		return new ArrayList<Ticket>(resultTickets);
	}

	public ArrayList<Ticket> getAllTickets() {
		return tickets;
	}
//	
//	public Boolean otkazi(String kupId, Integer manId) {  //vise karata? taj korisnik username
//		Manifestation m = ManifestationManager.getManifestacija(manId);
//		Period period = Period.between(m.getDatum(),  LocalDate.now());
//		Integer razlika = period.getDays();
//		
//		ArrayList<Ticket> oveBrisi = new ArrayList<Ticket>();
//		if(razlika >= 7) {
//			for(Ticket k : tickets) {
//				if (k.getKupacId().equals(kupId) && k.getManifestacijaId().equals(manId)) {
//					oveBrisi.add(k);
//				}
//			}
//			Integer ukupno_izgubljenih = 0;
//			for(Ticket b : oveBrisi) {
//				izbrisiKartu(b.getId());
//				int broj_izgubljenih_bodova = (int) (b.getCena()/1000 * 133 * 4);
//				ukupno_izgubljenih += broj_izgubljenih_bodova;
//			}
//		}
//		return false;
//	}
//
//	public ArrayList<Ticket> traziMan(String kupId, String substr){
//		ArrayList<Ticket> vrati = new ArrayList<Ticket>();
//		substr = substr.toLowerCase();
//		for (Ticket k :karteKupca(kupId)) {
//			String name = ManifestationManager.getManifestacija(k.getManifestacijaId()).getNaziv();
//			name = name.toLowerCase();
//			if(name.contains(substr)) {
//				vrati.add(k);
//			}
//		}
//		return vrati;
//	}
//	public ArrayList<Ticket> traziCena (String kupId,Double odc, Double doc){
//		ArrayList<Ticket> vrati = new ArrayList<Ticket>();
//		for (Ticket k :karteKupca(kupId)) {
//			if(k.getCena() <= doc && k.getCena() >= odc) {
//				vrati.add(k);
//			}
//		}
//		return vrati;
//	}
//	
////	public ArrayList<Karta> traziDatum (String kupId,Integer manId, LocalDate od, LocalDate do){
////		
////		ArrayList<Karta> vrati = new ArrayList<Karta>();
////		Manifestacija m = Manifestacije.getManifestacija(manId);
////		
////		for (Karta k :karteKupca(kupId)) {
////			if(m.getV>= d1.getTime() && middle.getTime() <= d2.getTime()) {
////				vrati.add(k);
////			}
////		}
////		//argument kupac id??? Mozda da li je nesto vec rezervisao pa da se ne prikazuje u pretrazi
////		
////		return vrati;
////	}
//	
//	public ArrayList<Ticket> filterTipKarte(String kupId, TicketType tip){
//		ArrayList<Ticket> vrati = new ArrayList<Ticket>();
//		for (Ticket k :karteKupca(kupId)) {
//			if (k.getTip().equals(tip)) {
//				vrati.add(k);
//			}
//		}
//		return vrati;
//	}
//	public ArrayList<Ticket> filterStatusKarte(String kupId,TicketStatus status){
//		ArrayList<Ticket> vrati = new ArrayList<Ticket>();
//		for (Ticket k :karteKupca(kupId)) {
//			if (k.getStatus().equals(status)) {
//				vrati.add(k);
//			}
//		}
//		return vrati;
//	}
//	
//	 
//	@SuppressWarnings("unchecked")
//	public ArrayList<Ticket> sortCena(Boolean rastuce){
//		Collections.sort(tickets,new CenaKomparator()); 
//		if(!rastuce) {
//			Collections.reverse(tickets);
//		}
//		 
//		return tickets;
//	}
//	@SuppressWarnings("rawtypes")
//	class CenaKomparator implements Comparator{  //najmanji prvi ja msm
//
//		@Override
//		public int compare(Object o1, Object o2) {
//			Ticket k1 = (Ticket)o1;
//			Ticket k2 = (Ticket)o2;
//			
//			if(k1.getCena()==k2.getCena()) {
//				return 0;
//			}else if(k1.getCena()> k2.getCena()) {
//				return 1;
//			}else {
//				return -1;
//			}
//		}
//	}
//	
//	
//	@SuppressWarnings("unchecked")
//	public ArrayList<Ticket> sortNaziv(Boolean rastuce){
//		Collections.sort(tickets,new NazivKomparator()); 
//		if(!rastuce) {
//			Collections.reverse(tickets);
//		}
//		 
//		return tickets;
//	}
//	@SuppressWarnings("rawtypes")
//	class NazivKomparator implements Comparator{  //najmanji prvi ja msm
//
//		@Override
//		public int compare(Object o1, Object o2) {
//			Ticket k1 = (Ticket)o1;
//			Manifestation m1 = ManifestationManager.getManifestacija(k1.getManifestacijaId());
//			Ticket k2 = (Ticket)o2;
//			Manifestation m2 = ManifestationManager.getManifestacija(k2.getManifestacijaId());
//			return m1.getNaziv().compareTo(m2.getNaziv());
//		}
//	}
//
//	
//	@SuppressWarnings("unchecked")
//	public ArrayList<Ticket> sortDatum(Boolean rastuce){
//		Collections.sort(tickets,new DatumKomparator()); 
//		if(!rastuce) {
//			Collections.reverse(tickets);
//		}
//		 
//		return tickets;
//	}
//	
//	@SuppressWarnings("rawtypes")
//	class DatumKomparator implements Comparator{  //najmanji prvi ja msm
//
//		@Override
//		public int compare(Object o1, Object o2) {
//			Ticket k1 = (Ticket)o1;
//			Manifestation m1 = ManifestationManager.getManifestacija(k1.getManifestacijaId());
//			LocalDate d1 = m1.getDatum();
//			Ticket k2 = (Ticket)o2;
//			Manifestation m2 = ManifestationManager.getManifestacija(k2.getManifestacijaId());
//			LocalDate d2 = m2.getDatum();
//			
//			if(d1.equals(d2)) {
//				return 0;
//			}else if(d1.isAfter(d2)) {
//				return 1;
//			}else {
//				return -1;
//			}
//
//		}
//	}
	
	
	public ArrayList<Ticket> getTickets() {
		List<Ticket> activeTickets =  tickets.stream().filter(t -> !t.getIsDeleted()).collect(Collectors.toList());
		return new ArrayList<Ticket>(activeTickets);
	}

	public Ticket getTicketById(String id) {
		Optional<Ticket> optional = tickets.stream().filter(t -> t.getId().equals(id) && !t.getIsDeleted()).findFirst();
		return optional.orElse(null);
	}
	
	public Boolean addTicket(Ticket ticket) {
		if(tickets.contains(ticket)) {
			return false;
		}
		tickets.add(ticket);
		IO.saveTickets(tickets);
		return true;
	}
	
	public Boolean izbrisiKartu(String id) {
		Ticket ticket = getTicketById(id);
		if(!tickets.contains(ticket)) {
			return false;
		}
		Integer index = tickets.indexOf(ticket);
		ticket.setIsDeleted(true);
		tickets.set(index, ticket);
		//TODO save file?
		return true;
	}

	public Integer getNumOfSoldTickets(Integer manId) {
		List<Ticket> resultTickets = tickets.stream()
				.filter(t -> t.getManifestation().equals(manId) 
							&& t.getTicketStatus().equals(TicketStatus.RESERVED)
							&& !t.getIsDeleted())
				.collect(Collectors.toList());
		
		return resultTickets.size();
	}
	
	public Integer getNumOfLeftoverTickets(Integer id) {
		Integer numOfSoldTickets = getNumOfSoldTickets(id);
		Integer numOfSeats = ManifestationManager.getInstance().getManifestationById(id).getNumOfAvailableSeats();
		
		return numOfSeats - numOfSoldTickets;
	}

	public void deleteTicket(Ticket ticket) {
		ticket.setIsDeleted(true);
	}
}
