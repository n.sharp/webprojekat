package managers;

import java.util.ArrayList;

import app.IO;
import entities.Location;

public class LocationManager {
	private ArrayList<Location> locations = new ArrayList<Location>();
	
	private static LocationManager instance;
	
	private LocationManager() {
		this.locations = IO.loadLocations();
	}
	
	public static LocationManager getInstance() {
		if(instance == null) {
			instance = new LocationManager();
		}
		return instance;
	}
	
	public ArrayList<Location> getAllLocations() {
		return locations;
	}
	
	public ArrayList<Location> getLocations() {
		ArrayList<Location> activeLocations = new ArrayList<Location>();
		
		for (Location location : locations) {
			if (!location.getIsDeleted()) {
				activeLocations.add(location);
			}
		}
		
		return activeLocations;
	}

	public Location getLocationById(Integer id) {
		locations = IO.loadLocations();
		for (Location location : locations) {
			if (location.getId().equals(id)
					&& !location.getIsDeleted()) {
				return location;
			}
		}
		return null;
	}
	
	public void addLocation(Location location) {
		locations.add(location);
		IO.saveLocations(locations);
	}
	
	public void deleteLocation(Location location) {
		location.setIsDeleted(true);
	}
}