package managers;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.gson.JsonElement;

import app.IO;
import dto.LoginDTO;
import dto.UserDTO;
import entities.Manifestation;
import entities.Ticket;
import users.Administrator;
import users.Buyer;
import users.BuyerType;
import users.Role;
import users.Seller;
import users.User;

public class UserManager {
	private ArrayList<User> users = new ArrayList<User>();
	private ArrayList<Administrator> administrators;
	private ArrayList<Buyer> buyers;
	private ArrayList<Seller> sellers;

	private static UserManager instance;
	
	private UserManager() {
		this.administrators = IO.loadAdministrators();
		this.users.addAll(this.administrators);
		
		this.buyers = IO.loadBuyers();
		this.users.addAll(this.buyers);
		
		this.sellers = IO.loadSellers();
		this.users.addAll(this.sellers);
		
	}
	
	public static UserManager getInstance() {
		if(instance == null) {
			instance = new UserManager();
		}
		return instance;
	}
	
	public ArrayList<User> getUsers(){
		return users;
	}
	
	public ArrayList<User> getActiveUsers(){
		ArrayList<User> activeUsers = new ArrayList<User>();
		
		activeUsers.addAll(getActiveAdministrators());
		activeUsers.addAll(getActiveBuyers());
		activeUsers.addAll(getActiveSellers());
		
		return activeUsers;
	}
	
	public ArrayList<Administrator> getAdministrators(){
		return administrators;
	}
	
	public ArrayList<Administrator> getActiveAdministrators() {
		ArrayList<Administrator> activeAdmins = new ArrayList<Administrator>();
		
		for (Administrator admin : administrators) {
			if (!admin.getIsDeleted()) {
				activeAdmins.add(admin);
			}
		}
		return activeAdmins;
	}

	public ArrayList<Buyer> getBuyers(){
		return buyers;
	}
	
	public ArrayList<Buyer> getActiveBuyers() {
		ArrayList<Buyer> activeBuyers = new ArrayList<Buyer>();
		
		for (Buyer buyer : buyers) {
			if (!buyer.getIsDeleted()) {
				activeBuyers.add(buyer);
			}
		}
		return activeBuyers;
	}

	public ArrayList<Seller> getSellers() {
		return sellers;
	}
	
	public ArrayList<Seller> getActiveSellers() {
		ArrayList<Seller> activeSellers = new ArrayList<Seller>();
		
		for (Seller seller : sellers) {
			if (!seller.getIsDeleted()) {
				activeSellers.add(seller);
			}
		}
		return activeSellers;
	}
	
	public User getUserByUsername(String username) {
		for(User user : users) {
			if(user.getUsername().equals(username) && !user.getIsDeleted()) {
				return user;
			}
		}
		return null;
	}
	
	public Administrator getAdministratorByUsername(String username) {
		for(Administrator administrator : administrators) {
			if(administrator.getUsername().equals(username) && !administrator.getIsDeleted()) {
				return administrator;
			}
		}
		return null;
	}

	public Buyer getBuyerByUsername(String username) {
		for (Buyer buyer : buyers) {
			if(buyer.getUsername().equals(username) && !buyer.getIsDeleted()) {
				return buyer;
			}
		}
		return null;
	}

	public Seller getSellerByUsername(String username) {
		for(Seller seller: sellers) {
			if(seller.getUsername().equals(username) && !seller.getIsDeleted()) {
				return seller;
			}
		}
		return null;
	}
	
	public ArrayList<Manifestation> getManifestationsBySeller(String username) {
		Seller seller = getSellerByUsername(username);
		ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
		
		for(Integer manifestationId : seller.getManifestations()) {
			ManifestationManager.getInstance();
			Manifestation manifestation = ManifestationManager.getInstance().getManifestationById(manifestationId);
			if(!manifestation.getIsDeleted()) {
				manifestations.add(manifestation);
			}
		}
		
		return manifestations;
	}
	
	public ArrayList<Integer> getManifestationsIdsBySeller(String username) {
		Seller seller = getSellerByUsername(username);
		ArrayList<Integer> manifestationsIds = new ArrayList<Integer>();
		
		for(Integer manifestationId : seller.getManifestations()) {
			ManifestationManager.getInstance();
			Manifestation manifestation = ManifestationManager.getInstance().getManifestationById(manifestationId);
			if(!manifestation.getIsDeleted()) {
				manifestationsIds.add(manifestationId);
			}
		}
		return manifestationsIds;
	}
	
	public ArrayList<Buyer> getBuyersBySellerUsername(String username) {
		ArrayList<Ticket> ticketsbySeller = TicketManager.getInstance().getReservedTicketsBySeller(username);
		
		List<String> buyersUsernames = ticketsbySeller.stream().map(t -> t.getBuyer()).collect(Collectors.toList());
		
		Set<String> hSet = new HashSet<String>();
        for (String x : buyersUsernames) {
            hSet.add(x);
        }
		List<Buyer> buyers = hSet.stream().map(uName -> getBuyerByUsername(uName)).collect(Collectors.toList());
		
		return new ArrayList<Buyer>(buyers);
	}
	
	public void addSeller(Seller seller) {
		sellers.add(seller);
		users.add(seller);
		IO.saveSellers(sellers);
	}
	
	public void deleteUser(User user) {
		user.setIsDeleted(true);
	}
	
	public void editAdministrator(Administrator newAdministrator) {
		Administrator oldAdministrator = getAdministratorByUsername(newAdministrator.getUsername());
		oldAdministrator.setPassword(newAdministrator.getPassword());
		oldAdministrator.setFirstName(newAdministrator.getFirstName());
		oldAdministrator.setLastName(newAdministrator.getLastName());
		oldAdministrator.setDateOfBirth(newAdministrator.getDateOfBirth());
		oldAdministrator.setGender(newAdministrator.getGender());
		oldAdministrator.setMoto(newAdministrator.getMoto());
	}
	
	public void editBuyer(Buyer newBuyer) {
		Buyer oldBuyer = getBuyerByUsername(newBuyer.getUsername());
		oldBuyer.setPassword(newBuyer.getPassword());
		oldBuyer.setFirstName(newBuyer.getFirstName());
		oldBuyer.setLastName(newBuyer.getLastName());
		oldBuyer.setDateOfBirth(newBuyer.getDateOfBirth());
		oldBuyer.setGender(newBuyer.getGender());
	}
	
	public void editSeller(Seller newSeller) {
		Seller oldSeller = getSellerByUsername(newSeller.getUsername());
		oldSeller.setPassword(newSeller.getPassword());
		oldSeller.setFirstName(newSeller.getFirstName());
		oldSeller.setLastName(newSeller.getLastName());
		oldSeller.setDateOfBirth(newSeller.getDateOfBirth());
		oldSeller.setGender(newSeller.getGender());
	}
	
	public boolean tryLogin(LoginDTO dto) {
		for(User user: users) {
			if(user.getUsername().equals(dto.getUsername())
					&& user.getPassword().equals(dto.getPassword())) {
				return true;
			}
		}
		return false;
	}
	
	public void addBuyer(Buyer buyer) {
		buyers.add(buyer);
		users.add(buyer);
		IO.saveBuyers(buyers);
	}
	
	public void addSellersManifestation(String username, Integer id) {
		Seller seller = getSellerByUsername(username);
		seller.getManifestations().add(id);
		IO.saveSellers(sellers);
	}

	public ArrayList<UserDTO> search(String firstName, String lastName, String username) {
		
		ArrayList<UserDTO> found = new ArrayList<UserDTO>();
		
		for(User user: users) {
			if( (firstName.equals("") || user.getFirstName().toLowerCase().contains(firstName))
					&& (lastName.equals("") || user.getLastName().toLowerCase().contains(lastName))
					&& (username.equals("") || user.getUsername().toLowerCase().contains(username))
					) {
				UserDTO dto = new UserDTO(user, null, null);
				if(user.getRole().equals(Role.BUYER)) {
					Buyer current = getBuyerByUsername(user.getUsername());
					dto.setPoints(current.getPoints());
					dto.setBuyerType(current.getBuyerType());
				}
			}
		}
		
		return found;
	}

	public void addPointsToBuyer(String username, Integer points) {
		Buyer b = getBuyerByUsername(username);
		Integer buyersInd = buyers.indexOf(b);
		Integer usersInd = users.indexOf(b);
		
		b.setPoints(b.getPoints() + points);
		
		if(b.getPoints() > 2000) {
			b.setBuyerType(BuyerType.BRONZE);
		}
		if(b.getPoints() > 3000) {
			b.setBuyerType(BuyerType.SILVER);
		}
		if(b.getPoints() > 4000) {
			b.setBuyerType(BuyerType.GOLD);
		}
		
		
		buyers.set(buyersInd, b);
		users.set(usersInd, b);
		IO.saveBuyers(buyers);
				
	}
}