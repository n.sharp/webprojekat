package managers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import app.IO;
import dto.ManifestationDTO;
import entities.Manifestation;

public class ManifestationManager {
	private ArrayList<Manifestation> manifestations = new ArrayList<Manifestation>();
	
	private static ManifestationManager instance;
	
	private ManifestationManager() {
		this.manifestations = IO.loadManifestations();
	}
	
	public static ManifestationManager getInstance() {
		if(instance == null) {
			instance = new ManifestationManager();
		}
		return instance;
	}
	
	public ArrayList<Manifestation> getManifestations() {
		ArrayList<Manifestation> notDeleted = new ArrayList<Manifestation>();
		for (Manifestation manifestation : manifestations) {
			if (!manifestation.getIsDeleted()) {
				notDeleted.add(manifestation);
			}
		}
		
		return notDeleted;
	}
	
	public ArrayList<Manifestation> getAllManifestations() {
		return manifestations;
	}
	
	public ArrayList<Manifestation> getActiveManifestations() {
		ArrayList<Manifestation> activeManifestations = new ArrayList<Manifestation>();
		
		for (Manifestation manifestation : getManifestations()) {
			if (manifestation.getIsActive()) {
				activeManifestations.add(manifestation);
			}
		}
		
		return activeManifestations;
	}

	public Manifestation getManifestationById(Integer id) {
		for (Manifestation manifestation : getAllManifestations()) {
			if (manifestation.getId() == id) {
				return manifestation;
			}
		}
		return null;
	}
	
	public ArrayList<Manifestation> getManifestationsByLocation(Integer id) {
		ArrayList<Manifestation> located = new ArrayList<Manifestation>();
		
		for (Manifestation manifestation : getManifestations()) {
			if (manifestation.getLocationId() == id) {
				located.add(manifestation);
			}
		}
		
		return located;
	}

	public void addManifestation(Manifestation manifestation) {
		manifestations.add(manifestation);
		IO.saveManifestations(manifestations);
	}
	
	public void deleteManifestation(Manifestation manifestation) {
		manifestation.setIsDeleted(true);
		IO.saveManifestations(manifestations);
	}
	
	public ArrayList<ManifestationDTO> searchManfestations(String name, String dateFrom, String dateTo, String location, Double priceFrom, Double priceTo) {
		ArrayList<ManifestationDTO> found = new ArrayList<ManifestationDTO>();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocationManager lm = LocationManager.getInstance();
		
		for (Manifestation manifestation : getManifestations()) {
			if ((name.equals("") || manifestation.getName().toLowerCase().contains(name.toLowerCase())) &&
				(dateFrom.equals("") || LocalDate.parse(dateFrom, dtf).atStartOfDay().isBefore(manifestation.getDateTime())) && 
				(dateTo.equals("") || LocalDate.parse(dateTo, dtf).atStartOfDay().isAfter(manifestation.getDateTime())) &&
				(location.equals("") || lm.getLocationById(manifestation.getLocationId()).getCity().toLowerCase().contains(location.toLowerCase())) && 
				(priceFrom == -1 || manifestation.getRegularTicketPrice() >= priceFrom) && 
				(priceTo == -1 || manifestation.getRegularTicketPrice() <= priceTo)) {
				found.add(new ManifestationDTO(manifestation));
			}
		}
		
		return found;
	}
	
	public void editManifestation(Manifestation newM) {
		Manifestation oldM = getManifestationById(newM.getId());
		oldM.setDateTime(newM.getDateTime());
		oldM.setName(newM.getName());
		oldM.setRegularTicketPrice(newM.getRegularTicketPrice());
		oldM.setManifestationType(newM.getManifestationType());
		oldM.setNumOfAvailableSeats(newM.getNumOfAvailableSeats());
		oldM.setLocationId(newM.getLocationId());
	}
	
	public Manifestation updateManifestation(Integer id, Manifestation manifestation) {
		Manifestation original = getManifestationById(id);
		Integer position = getManifestations().indexOf(original);
		
		manifestation.setId(id);
		manifestation.setIsDeleted(false);
		manifestation.setIsActive(false); //TODO proveriti zasto false
		
		manifestations.set(position, manifestation);
		
		//sacuvati u FAJL
		
//		manifestation.setName(newManifestation.getName());
//		manifestation.setManifestationType(newManifestation.getManifestationType());
//		manifestation.setNumOfAvailableSeats(newManifestation.getNumOfAvailableSeats());
//		manifestation.setDate(newManifestation.getDate());
//		manifestation.setTime(newManifestation.getTime());
//		
//		manifestation.setRegularTicketPrice(newManifestation.getRegularTicketPrice());
//		manifestation.setIsActive(false); //TODO proveriti zasto false
//		manifestation.setLocation(newManifestation.getLocation());
//		manifestation.setPosterImagePath(newManifestation.getPosterImagePath());
//		manifestation.setIsDeleted(false);

		return manifestation;
	}

}
