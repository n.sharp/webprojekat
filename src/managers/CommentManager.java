package managers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import app.IO;
import entities.Comment;
import entities.CommentStatus;

public class CommentManager {
	private ArrayList<Comment> comments;
	
	private static CommentManager instance;
	
	private CommentManager() {
		this.comments = IO.loadComments();
	}
	
	public static CommentManager getInstance() {
		if(instance == null) {
			instance = new CommentManager();
		}
		return instance;
	}
	
	public ArrayList<Comment> getComments() {
		ArrayList<Comment> notDeleted = new ArrayList<Comment>();
		
		for (Comment comment : comments) {
			if (!comment.getIsDeleted()) {
				notDeleted.add(comment);
			}
		}
		
		return notDeleted;
	}
	
	public ArrayList<Comment> getAllComments() {
		return comments;
	}

	public void setComments(ArrayList<Comment> comments) {
		this.comments = comments;
	}

	public ArrayList<Comment> getCommentsByManifestation(Integer id){
		ArrayList<Comment> resultComments = new ArrayList<Comment>();
		
		for (Comment comment : getComments()) {
			if(comment.getManifestation() == id) {
				resultComments.add(comment);
			}
		}
		
		return resultComments;
	}
	
	public ArrayList<Comment> getCreatedComments(){
		ArrayList<Comment> created = new ArrayList<Comment>();
		
		for (Comment comment : getComments()) {
			if(comment.getCommentStatus() == CommentStatus.CREATED) {
				created.add(comment);
			}
		}
		
		return created;
	}
	
	public ArrayList<Comment> getApprovedCommentsForManifestation (Integer id){
		ArrayList<Comment> resultComments = new ArrayList<Comment>();
		
		for (Comment comment : getComments()) {
			if(comment.getManifestation() == id && comment.getCommentStatus().equals(CommentStatus.APPROVED)) {
				resultComments.add(comment);
			}
		}
		return resultComments;
	}
	
	public ArrayList<Comment> getCreatedCommentsForSellersManifestations(String username){
		ArrayList<Comment> comments = new ArrayList<Comment>();
		ArrayList<Integer> sellersManifestations= UserManager.getInstance().getManifestationsIdsBySeller(username);
		
		for (Comment comment : getComments()) {
			if(comment.getCommentStatus().equals(CommentStatus.CREATED) && sellersManifestations.contains(comment.getManifestation())) {
				comments.add(comment);
			}
		}
		return comments;
	}

	public Double getAverageGradeForManifestation(Integer manId) {
		List<Integer> grades = getApprovedCommentsForManifestation(manId)
				.stream()
				.map(comment -> comment.getGrade())
				.collect(Collectors.toList());
		
		if(grades.size() == 0) {
			return 0.0;
		}
		
		Integer sum = grades.stream()
				  .reduce(0, (a, b) -> a + b);
		
		return ((double) sum) / grades.size();
	}

	public void deleteComment(Comment comment) {
		for (Comment c : comments) {
			if (c.getAuthor().equals(comment.getAuthor()) && c.getManifestation() == comment.getManifestation()) {
				c.setIsDeleted(true);
				return;
			}
		}
	}
	
	public void approveComment(Comment comment) {
		for (Comment c : comments) {
			if (c.getAuthor().equals(comment.getAuthor()) && c.getManifestation() == comment.getManifestation()) {
				c.setCommentStatus(CommentStatus.APPROVED);
				return;
			}
		}
	}
	
	public void rejectComment(Comment comment) {
		for (Comment c : comments) {
			if (c.getAuthor().equals(comment.getAuthor()) && c.getManifestation() == comment.getManifestation()) {
				c.setCommentStatus(CommentStatus.REJECTED);
				return;
			}
		}
	}
}
