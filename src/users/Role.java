package users;

public enum Role {
	ADMINISTRATOR,
	SELLER,
	BUYER
}
