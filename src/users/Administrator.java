package users;

import java.time.LocalDate;

public class Administrator extends User {
	private String moto;

	public Administrator() {
		super();
	}

	public Administrator(String moto) {
		super();
		this.moto = moto;
	}

	public Administrator(String username, String password, String firstName, String lastName, Gender gender,
			LocalDate dateOfBirth, Role role, Boolean isDeleted, String moto) {
		super(username, password, firstName, lastName, gender, dateOfBirth, role, isDeleted);
		this.moto = moto;
	}

	public String getMoto() {
		return moto;
	}

	public void setMoto(String moto) {
		this.moto = moto;
	}
}
