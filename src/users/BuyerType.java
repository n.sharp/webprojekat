package users;

public enum BuyerType {
	GOLD,
	SILVER,
	BRONZE,
	REGULAR
}
