 package users;

import java.time.LocalDate;
import java.util.List;

public class Buyer extends User {
	private List<String> tickets; 
	private Integer points;
	private BuyerType buyerType;
	private Boolean isBlocked;
	
	public Buyer() {
		super();
	}
	
	public Buyer(String username, String password, String firstName, String lastName, Gender gender,
			LocalDate dateOfBirth, Role role, Boolean isDeleted, List<String> tickets, Integer points,
			BuyerType buyerType, Boolean isBlocked) {
		super(username, password, firstName, lastName, gender, dateOfBirth, role, isDeleted);
		this.tickets = tickets;
		this.points = points;
		this.buyerType = buyerType;
		this.isBlocked = isBlocked;
	}

	public Buyer(List<String> tickets, Integer points, BuyerType buyerType, Boolean isBlocked) {
		super();
		this.tickets = tickets;
		this.points = points;
		this.buyerType = buyerType;
		this.isBlocked = isBlocked;
	}

	public List<String> getTickets() {
		return tickets;
	}

	public void setTickets(List<String> tickets) {
		this.tickets = tickets;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public BuyerType getBuyerType() {
		return buyerType;
	}

	public void setBuyerType(BuyerType buyerType) {
		this.buyerType = buyerType;
	}

	public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}
	
	/*public BuyerType(TypeName typeName) {
		super();
		this.typeName = typeName;
		
		switch(typeName) {
		
		case GOLD:
			this.discount = 15;
			this.requiredPoints =1500;
			break;
			
		case SILVER:
			this.discount = 10;
			this.requiredPoints =1000;
			break;
			
		case BRONZE:
			this.discount = 5 ;
			this.requiredPoints =500;
			break;
			
		case REGULAR:
			this.discount = 0;
			this.requiredPoints = 0;
			break;
			
		default:
			this.discount = 0;
			this.requiredPoints = 0;
			break;
		}
	}*/
}
