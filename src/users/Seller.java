package users;

import java.time.LocalDate;
import java.util.ArrayList;

public class Seller extends User {

	private ArrayList<Integer> manifestations;

	public Seller() {
		super();
	}

	public Seller(ArrayList<Integer> manifestations) {
		super();
		this.manifestations = manifestations;
	}

	public Seller(String username, String password, String firstName, String lastName, Gender gender,
			LocalDate dateOfBirth, Role role, Boolean isDeleted, ArrayList<Integer> manifestations) {
		super(username, password, firstName, lastName, gender, dateOfBirth, role, isDeleted);
		this.manifestations = manifestations;
	}

	public ArrayList<Integer> getManifestations() {
		return manifestations;
	}

	public void setManifestations(ArrayList<Integer> manifestations) {
		this.manifestations = manifestations;
	}
	
}
