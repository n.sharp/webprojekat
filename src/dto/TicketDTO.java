package dto;

import java.time.LocalDateTime;

import entities.Ticket;
import entities.TicketStatus;
import entities.TicketType;

public class TicketDTO {
	private String id;
	private String manifestation;
	private LocalDateTime dateTime;
	private Double price;
	private String buyer;
	private TicketStatus ticketStatus;
	private TicketType ticketType;
	
	public TicketDTO() {
		super();
	}

	public TicketDTO(Ticket ticket, String manifestation, LocalDateTime dateTime) {
		this.id = ticket.getId();
		this.manifestation = manifestation;
		this.dateTime = dateTime;
		this.price = ticket.getPrice();
		this.buyer = ticket.getBuyer();
		this.ticketStatus = ticket.getTicketStatus();
		this.ticketType = ticket.getTicketType();
	}
	
	public TicketDTO(String id, String manifestation, LocalDateTime dateTime, Double price, String buyer,
			TicketStatus ticketStatus, TicketType ticketType) {
		super();
		this.id = id;
		this.manifestation = manifestation;
		this.dateTime = dateTime;
		this.price = price;
		this.buyer = buyer;
		this.ticketStatus = ticketStatus;
		this.ticketType = ticketType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getManifestation() {
		return manifestation;
	}

	public void setManifestation(String manifestation) {
		this.manifestation = manifestation;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}
}