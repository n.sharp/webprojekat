package dto;

public class CurrentUserDTO {
	public boolean isLoggedIn;
	public String role;
	public String username;
	
	public CurrentUserDTO() {
		isLoggedIn = false;
		role = null;
		username = null;
	}

}
