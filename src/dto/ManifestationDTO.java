package dto;

import java.time.LocalDateTime;

import entities.Location;
import entities.Manifestation;
import entities.ManifestationType;
import managers.CommentManager;
import managers.LocationManager;
import managers.TicketManager;

public class ManifestationDTO {
	private Integer id;
	private String name;
	private ManifestationType manifestationType;
	private Integer numOfAvailableSeats;
	private LocalDateTime dateTime;
	private Double regularTicketPrice;
	private Boolean isActive;
	private String posterImagePath;

	//location info
	private Double longitude;
	private Double latitude;
	private String street;
	private String city;
	private String postalNumber;
	
	//other
	private Double averageGrade;
	private Integer numOfTicketsLeft;
	
	public ManifestationDTO(Manifestation m) {
		this.id = m.getId();
		this.name = m.getName();
		this.manifestationType = m.getManifestationType();
		this.numOfAvailableSeats = m.getNumOfAvailableSeats();
		this.dateTime = m.getDateTime();
		this.regularTicketPrice = m.getRegularTicketPrice();
		this.isActive = m.getIsActive();
		this.posterImagePath = m.getPosterImagePath();
		
		LocationManager lm = LocationManager.getInstance();

		Location location = lm.getLocationById(m.getLocationId());
		this.street = location.getStreet();
		this.city = location.getCity();
		this.postalNumber = location.getPostalNumber();
		this.longitude = location.getLongitude();
		this.latitude = location.getLatitude();
		System.out.println("OVDE KONSTRUKTOR< SACE KOMENTARI");
		this.averageGrade = CommentManager.getInstance().getAverageGradeForManifestation(m.getId());
		this.numOfTicketsLeft = TicketManager.getInstance().getNumOfLeftoverTickets(m.getId());
	}
	

	public Integer getNumOfTicketsLeft() {
		return numOfTicketsLeft;
	}


	public void setNumOfTicketsLeft(Integer numOfTicketsLeft) {
		this.numOfTicketsLeft = numOfTicketsLeft;
	}


	public ManifestationDTO() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ManifestationType getManifestationType() {
		return manifestationType;
	}

	public void setManifestationType(ManifestationType manifestationType) {
		this.manifestationType = manifestationType;
	}

	public Integer getNumOfAvailableSeats() {
		return numOfAvailableSeats;
	}

	public void setNumOfAvailableSeats(Integer numOfAvailableSeats) {
		this.numOfAvailableSeats = numOfAvailableSeats;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public Double getRegularTicketPrice() {
		return regularTicketPrice;
	}

	public void setRegularTicketPrice(Double regularTicketPrice) {
		this.regularTicketPrice = regularTicketPrice;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPosterImagePath() {
		return posterImagePath;
	}

	public void setPosterImagePath(String posterImagePath) {
		this.posterImagePath = posterImagePath;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalNumber() {
		return postalNumber;
	}

	public void setPostalNumber(String postalNumber) {
		this.postalNumber = postalNumber;
	}

	public Double getAverageGrade() {
		return averageGrade;
	}

	public void setAverageGrade(Double averageGrade) {
		this.averageGrade = averageGrade;
	}
}
