package entities;

public class Ticket {
	private String id;
	private Integer manifestation;
	private Double price;
	private String buyer;
	private TicketStatus ticketStatus;
	private TicketType ticketType;
	private Boolean isDeleted;
	
	public Ticket() {
		super();
	}

	public Ticket(String id, Integer manifestation, Double price, String buyer, TicketStatus ticketStatus,
			TicketType ticketType, Boolean isDeleted) {
		super();
		this.id = id;
		this.manifestation = manifestation;
		this.price = price;
		this.buyer = buyer;
		this.ticketStatus = ticketStatus;
		this.ticketType = ticketType;
		this.isDeleted = isDeleted;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getManifestation() {
		return manifestation;
	}

	public void setManifestation(Integer manifestation) {
		this.manifestation = manifestation;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}
