package entities;

public enum TicketStatus {
	RESERVED, 
	CANCELED
}
