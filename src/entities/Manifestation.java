package entities;

import java.time.LocalDateTime;

public class Manifestation {
	private Integer id;
	private String name;
	private ManifestationType manifestationType;
	private Integer numOfAvailableSeats;
	private LocalDateTime dateTime;
	private Double regularTicketPrice;
	private Boolean isActive;
	private Integer locationId;
	private String posterImagePath;
	private Boolean isDeleted;
	
	public Manifestation() {
		super();
	}

	public Manifestation(Integer id, String name, ManifestationType manifestationType, Integer numOfAvailableSeats,
			LocalDateTime dateTime, Double regularTicketPrice, Boolean isActive, Integer locationId,
			String posterImagePath, Boolean isDeleted) {
		super();
		this.id = id;
		this.name = name;
		this.manifestationType = manifestationType;
		this.numOfAvailableSeats = numOfAvailableSeats;
		this.dateTime = dateTime;
		this.regularTicketPrice = regularTicketPrice;
		this.isActive = isActive;
		this.locationId = locationId;
		this.posterImagePath = posterImagePath;
		this.isDeleted = isDeleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ManifestationType getManifestationType() {
		return manifestationType;
	}

	public void setManifestationType(ManifestationType manifestationType) {
		this.manifestationType = manifestationType;
	}

	public Integer getNumOfAvailableSeats() {
		return numOfAvailableSeats;
	}

	public void setNumOfAvailableSeats(Integer numOfAvailableSeats) {
		this.numOfAvailableSeats = numOfAvailableSeats;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	public Double getRegularTicketPrice() {
		return regularTicketPrice;
	}

	public void setRegularTicketPrice(Double regularTicketPrice) {
		this.regularTicketPrice = regularTicketPrice;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getPosterImagePath() {
		return posterImagePath;
	}

	public void setPosterImagePath(String posterImagePath) {
		this.posterImagePath = posterImagePath;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
}
