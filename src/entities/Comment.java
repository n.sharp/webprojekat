package entities;

public class Comment {
	private String author;
	private Integer manifestation;
	private String text;
	private Integer grade; //1-5
	private Boolean isDeleted;
	private CommentStatus commentStatus;
	
	public Comment() {
		super();
	}

	public Comment(String author, Integer manifestation, String text, Integer grade, Boolean isDeleted,
			CommentStatus commentStatus) {
		super();
		this.author = author;
		this.manifestation = manifestation;
		this.text = text;
		this.grade = grade;
		this.isDeleted = isDeleted;
		this.commentStatus = commentStatus;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getManifestation() {
		return manifestation;
	}

	public void setManifestation(Integer manifestation) {
		this.manifestation = manifestation;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public CommentStatus getCommentStatus() {
		return commentStatus;
	}

	public void setCommentStatus(CommentStatus commentStatus) {
		this.commentStatus = commentStatus;
	}
}
