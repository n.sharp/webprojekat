package entities;

public enum CommentStatus {
	CREATED,
	APPROVED,
	REJECTED
}
