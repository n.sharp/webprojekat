package entities;

public class Location {
	
	private Integer id;
	private Double longitude;
	private Double latitude;
	private String street;
	private String city;
	private String postalNumber;
	private Boolean isDeleted;
	
	public Location() {
		
	}

	public Location(Integer id, Double longitude, Double latitude, String street, String city, String postalNumber,
			Boolean isDeleted) {
		super();
		this.id = id;
		this.longitude = longitude;
		this.latitude = latitude;
		this.street = street;
		this.city = city;
		this.postalNumber = postalNumber;
		this.isDeleted = isDeleted;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostalNumber() {
		return postalNumber;
	}

	public void setPostalNumber(String postalNumber) {
		this.postalNumber = postalNumber;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}
