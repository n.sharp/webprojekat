package entities;

public enum ManifestationType {
	CONCERT, 
	FESTIVAL,
	SHOW,
	OPERA,
	SPORT
}
