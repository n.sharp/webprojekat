package app;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


public class DateAdapter implements JsonDeserializer<LocalDate> {

	@Override
	public LocalDate deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		LocalDate dateObject = LocalDate.parse(json.getAsString(), DateTimeFormatter.ofPattern("dd-MM-yyyy"));
				LocalTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("HH:mm"));
		return dateObject;
	}
	

}