package app;

import java.lang.reflect.Type;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

class TimeAdapter implements JsonDeserializer<LocalTime>{

	@Override
	public LocalTime deserialize(JsonElement json, Type arg1, JsonDeserializationContext arg2)
			throws JsonParseException {
		
		LocalTime timeObject = LocalTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("HH:mm"));
		
		return timeObject;
	}


	
}