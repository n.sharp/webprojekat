package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import entities.Comment;
import entities.Location;
import entities.Manifestation;
import entities.Ticket;
import users.Administrator;
import users.Buyer;
import users.Seller;

public class IO {
	private static JsonDeserializer<LocalDateTime> deser1 =  new JsonDeserializer<LocalDateTime>() { 
		@Override 
		public LocalDateTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException { 
			return LocalDateTime.parse(json.getAsString(), DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm")); 
		}};
		
	private static JsonSerializer<LocalDateTime> ser1 = new JsonSerializer<LocalDateTime>() {
		@Override
		public JsonElement serialize(LocalDateTime src, Type typeOfSrc, JsonSerializationContext context) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm");
			return new JsonPrimitive(src.format(formatter));
		}};
		
	private static Gson gsonDateTime = new GsonBuilder()
			.registerTypeAdapter(LocalDateTime.class, ser1)
			.registerTypeAdapter(LocalDateTime.class, deser1)
			.create();
	
	
	private static JsonDeserializer<LocalDate> deser =  new JsonDeserializer<LocalDate>() { 
		@Override 
		public LocalDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException { 
			return LocalDate.parse(json.getAsString(), DateTimeFormatter.ofPattern("dd.MM.yyyy.")); 
		}};
		
	private static JsonSerializer<LocalDate> ser = new JsonSerializer<LocalDate>() {
		@Override
		public JsonElement serialize(LocalDate src, Type typeOfSrc, JsonSerializationContext context) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy.");
			return new JsonPrimitive(src.format(formatter));
		}
	};
		
	private static Gson gsonDate = new GsonBuilder()
			.registerTypeAdapter(LocalDate.class, ser)
			.registerTypeAdapter(LocalDate.class, deser)
			.create();
	
	private static String ADMINISTRATORS_FILEPATH = "static/Data/administrators.json";
	private static String BUYERS_FILEPATH = "static/Data/buyers.json";
	private static String SELLERS_FILEPATH = "static/Data/sellers.json";
	private static String LOCATIONS_FILEPATH = "static/Data/locations.json";
	private static String MANIFESTATIONS_FILEPATH = "static/Data/manifestations.json";
	private static String COMMENTS_FILEPATH = "static/Data/comments.json";
	private static String TICKETS_FILEPATH = "static/Data/tickets.json";
	
//GENERAL FUNCTIONS	
	
	private static void writeToFile(File f, String content) {
		
		if(!f.exists()) {
			try {
				f.createNewFile();
				
			} catch(IOException e) {
				System.out.println("Error during file creation.");
				e.printStackTrace();
			}
		}
		
		PrintWriter writer;
		
		try {
			writer = new PrintWriter(new FileOutputStream(f));
			writer.write(content);
			writer.close();
		} catch(FileNotFoundException e) {
			System.out.println("PrintWriter error.");
			e.printStackTrace();
		}
	}
	
	private static String loadFile(File file) throws FileNotFoundException {
		String fileContent = "";
		
		try {
			fileContent = new String(Files.readAllBytes(file.toPath()));
		}catch(IOException e) {
			System.out.println("Error while loading file: "+ file.getAbsolutePath());
			e.printStackTrace();
		}
		
		return fileContent;
	}
	
//SAVE DATA	
	
	public static void saveAdministrators(ArrayList<Administrator> administrators) {
		String json = gsonDate.toJson(administrators);
		File file = new File(ADMINISTRATORS_FILEPATH);
		writeToFile(file, json);
	}
	
	public static void saveBuyers (ArrayList<Buyer> buyers) {
		String json = gsonDate.toJson(buyers);
		File file = new File(BUYERS_FILEPATH);
		writeToFile(file, json);
	}
	
	public static void saveSellers (ArrayList<Seller> sellers) {
		String json = gsonDate.toJson(sellers);
		File file = new File(SELLERS_FILEPATH);
		writeToFile(file, json);
	}
	
	public static void saveLocations (ArrayList<Location> locations) {
		String json = gsonDateTime.toJson(locations);
		File file = new File(LOCATIONS_FILEPATH);
		writeToFile(file, json);
	}
	
	public static void saveManifestations (ArrayList<Manifestation> manifestations) {
		String json = gsonDateTime.toJson(manifestations);
		File file = new File(MANIFESTATIONS_FILEPATH);
		writeToFile(file, json);
	}
	
	public static void saveComments (ArrayList<Comment> comments) {
		String json = gsonDateTime.toJson(comments);
		File file = new File(COMMENTS_FILEPATH);
		writeToFile(file, json);
	}
	
	public static void saveTickets (ArrayList<Ticket> tickets) {
		String json = gsonDateTime.toJson(tickets);
		File file = new File(TICKETS_FILEPATH);
		writeToFile(file, json);
	}

//LOAD DATA	
	
	public static ArrayList<Administrator> loadAdministrators() {
		String json;
		
		try {
			json = loadFile(new File(ADMINISTRATORS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Administrator>();
		}
		
		Administrator[] dataArray = gsonDate.fromJson(json, Administrator[].class);
		ArrayList<Administrator> dataList = new ArrayList<Administrator>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
	
	public static ArrayList<Seller> loadSellers() {
		String json;
		
		try {
			json = loadFile(new File(SELLERS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Seller>();
		}
		
		Seller[] dataArray = gsonDate.fromJson(json, Seller[].class);
		ArrayList<Seller> dataList = new ArrayList<Seller>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
	
	public static ArrayList<Buyer> loadBuyers() {
		String json;
		
		try {
			json = loadFile(new File(BUYERS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Buyer>();
		}
		
		Buyer[] dataArray = gsonDate.fromJson(json, Buyer[].class);
		ArrayList<Buyer> dataList = new ArrayList<Buyer>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
	
	public static ArrayList<Comment> loadComments() {
		String json;
		
		try {
			json = loadFile(new File(COMMENTS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Comment>();
		}
		
		Comment[] dataArray = gsonDateTime.fromJson(json, Comment[].class);
		ArrayList<Comment> dataList = new ArrayList<Comment>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
	
	public static ArrayList<Ticket> loadTickets() {
		String json;
		
		try {
			json = loadFile(new File(TICKETS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Ticket>();
		}
		
		Ticket[] dataArray = gsonDateTime.fromJson(json, Ticket[].class);
		ArrayList<Ticket> dataList = new ArrayList<Ticket>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
	
	public static ArrayList<Location> loadLocations() {
		String json;
		
		try {
			json = loadFile(new File(LOCATIONS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Location>();
		}
		
		Location[] dataArray = gsonDateTime.fromJson(json, Location[].class);
		ArrayList<Location> dataList = new ArrayList<Location>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
	
	public static ArrayList<Manifestation> loadManifestations() {
		String json;
		
		try {
			json = loadFile(new File(MANIFESTATIONS_FILEPATH));
		}catch( FileNotFoundException e) {
			return new ArrayList<Manifestation>();
		}
		
		Manifestation[] dataArray = gsonDateTime.fromJson(json, Manifestation[].class);
		ArrayList<Manifestation> dataList = new ArrayList<Manifestation>();
		
		Collections.addAll(dataList, dataArray);
		return dataList;
	}
}
