package app;
//import static spark.Spark.port;
//import static spark.Spark.post;
//import static spark.Spark.staticFiles;
import static spark.Spark.before;
import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFiles;

import java.io.File;
import java.io.IOException;

import controllers.CommentController;
import controllers.LocationController;
import controllers.ManifestationController;
import controllers.TicketController;
import controllers.UserController;
import spark.Filter;

public class Main {

	public static void main(String[] args) throws IOException {
		port(8080);

		staticFiles.externalLocation(new File("./static").getCanonicalPath());
		
		get("api/userInfo", UserController.getUserInfo);
		post("api/login", UserController.verifyLogin);
		post("api/register", UserController.registerBuyer);
		
		before("api/users/*", authenticate);
		
		get("api/users/logout", UserController.logout);
		get("api/users/users", UserController.getUsers);
		get("api/users/admins", UserController.getAdministrators);
		get("api/users/buyers", UserController.getBuyers);
		get("api/users/buyersBySeller", UserController.getBuyersBySellerUsername);
		get("api/users/sellers", UserController.getSellers);
		post("api/users/addSeller", UserController.addSeller);
		get("api/users/deleteUser", UserController.deleteUser);
		get("api/users/userProfile", UserController.getUser);
		post("api/users/editUser", UserController.editUser);
		get("api/users/search", UserController.search);
		
//LOCATIONS
		before("api/locations/*", authenticate);
		
		get("api/locations/locations", LocationController.getLocations);
		post("api/locations/getIdOfLocationOrCreate", LocationController.getIdOfLocationOrCreate);
		
//MANIFESTATIONS //no auth for getAllManifestations, the other man methods have authentication inside themselves
		get("api/manifestations/manifestations", ManifestationController.getManifestations);
		get("api/manifestations/sellersManifestations", ManifestationController.getManifestationsBySeller);
		get("api/manifestations/inactive", ManifestationController.getInactiveManifestations);
		get("api/manifestations/active", ManifestationController.getActiveManifestations);
		get("api/manifestations/approve", ManifestationController.approveManifestation);
		get("api/manifestations/search", ManifestationController.searchManifestations);
		post("api/manifestations/createManifestation", ManifestationController.createManifestation);
		get("api/manifestations/manifestation/:id", ManifestationController.getManifestationDTOById);
		get("api/manifestations/deleteManifestation", ManifestationController.deleteManifestation);
		post("api/manifestations/editManifestation", ManifestationController.editManifestation);
		
//TICKETS
		before("api/tickets/*", authenticate);
		get("api/tickets/calculate", TicketController.calculatePriceNew);
		get("api/tickets/buyTickets", TicketController.buyTickets);
		get("api/tickets/tickets", TicketController.getTickets);
		get("api/tickets/searchTickets", TicketController.searchTickets);
		get("api/tickets/deleteTicket", TicketController.deleteTicket);
		
//COMMENTS
		get("api/comments/manifestationComments", CommentController.getCommentsByManifestation);
		post("api/comments/deleteComment", CommentController.deleteComment);
		get("api/comments/getCreated", CommentController.getCreatedComments);
		post("api/comments/approveReject", CommentController.approveRejectComment);
		
//LOCATIONS
		get("api/locations/locations", LocationController.getLocations);
		get("api/locations/deleteLocation", LocationController.deleteLocation);
    }
	
	static Filter authenticate = (req, res) -> {
		if(req.session(false) == null) {
			halt(401, "User not signed in!");
		}
	};

}
