const login = {template: '<login></login>'}
const manifestations = {template: '<manifestations></manifestations>'}
const registration = {template: '<registration></registration>'}
const frontpage = {template: '<frontpage></frontpage>'}
const users = {template: '<users></users>'}
const sellersManifestations = {template: '<sellersManifestations></sellersManifestations>'}
const addSeller = {template: '<addSeller></addSeller>'}
const tickets = {template: '<tickets></tickets>'}
const my_layout = {template: '<my_layout></my_layout>'}
const userProfile = {template: '<userProfile></userProfile>'}
const manifestation = {template: '<manifestation></manifestation>'}
const editManifestation = {template: '<editManifestation></editManifestation>'}
const createManif = {template: '<createManif></createManif>'}
const approveManifestations = {template: '<approveManifestations></approveManifestations>'}
const approveComments = {template: '<approveComments></approveComments>'}
const locations = {template: '<locations></locations>'}
const buyTickets = {template: '<buyTickets></buyTickets>'}
const buyers = {template: '<buyers></buyers>'}

const router = new VueRouter({
  mode: 'hash',
  routes: [
    { path: '/', component: frontpage},
    { path: '/frontPage', component: frontpage},
    { path: '/login', component: login },
    { path: '/registration', component: registration },
    { path: '/users', component: users },
    { path: '/sellersManifestations', component: sellersManifestations },
    { path: '/addSeller', component: addSeller },
    { path: '/tickets', component: tickets },
    { path: '/userProfile', component: userProfile },
    { path: '/createManif', component: createManif },
    { path: '/manifestation/:id', component: manifestation },
    { path: '/editManifestation/:id', component: editManifestation },
    { path: '/approveComments', component: approveComments },
    { path: '/locations', component: locations },
    { path: '/approveManifestations', component: approveManifestations},
    { path: '/buyTickets', component: buyTickets},
    { path: '/buyers', component: buyers}
  ]
})

router.beforeEach((to, from, next) => { 
  var currentUser;

  axios.get("api/userInfo")
  .then(response => {
    currentUser = response.data;

    if(!currentUser.isLoggedIn) {
      if(to.path === "/" || to.path ==="/login" || to.path === "/registration" || to.path === "/manifestation/" + to.params.id) { 
        next();
      } else if(to.path === "/frontPage") {
        localStorage.setItem("role", null);
        localStorage.setItem("username", "");
        next();
      } else{
        next("/");
      }
        
    } else{
      localStorage.setItem("role", currentUser.role);
      localStorage.setItem("username", currentUser.username);
      next();
    }

  })
  .catch(function(error) {
    alert(error.response.data);
  });
})


var app = new Vue({
  router,
  el: '#ticketMaster'
});

