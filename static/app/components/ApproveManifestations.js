Vue.component("approveManifestations", {
  data: function () {
    return {
      manifestations: [],
      hasManifestations: false
    }
  },

  template: `
  <my_layout>
  <div class="text-center mx-5 row" v-if="hasManifestations">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Image</th>
            <th class="sortable" v-on:click="sort('name')">Name</th>
            <th>Type</th>
            <th class="sortable" v-on:click="sort('dateTime')">Date and time</th>
            <th class="sortable" v-on:click="sort('price')">Regular ticket price</th>
            <th class="sortable" v-on:click="sort('location')">Location</th>
            <th>Average grade</th>
            <th>Approve</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="manifestation in manifestations">
            <td width="15%">
              <img :src="manifestation.posterImagePath" width="100%" alt="Picture" style="display:block;"/>
            </td>
            <td>{{manifestation.name}}</td>
            <td>{{manifestation.manifestationType}}</td>
            <td>{{manifestation.dateTime}}</td>
            <td>{{manifestation.regularTicketPrice}}</td>
            <td>{{manifestation.location}}</td>
            <td>{{manifestation.averageGrade}}</td>
            <td>
              <a href="#" v-on:click="approve(manifestation.id)">Approve</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

  <div v-else class="row justify-content-center">
    <h4>There is no inactive manifestations</h4>
  </div>
</my_layout>
  `,

  mounted() {
    //getuj neaktivne
    axios.get("/api/manifestations/inactive")
      .then(response => {
        console.log(response.data)

        var manifestations = [];
        response.data.forEach((manifestation) => {
          var grade;
          if (manifestation.averageGrade == 0) {
            console.log("EVO NULE")
            grade = null;
          } else {
            grade = parseFloat(manifestation.averageGrade).toFixed(2);
          }

          manifestations.push({
            id: manifestation.id,
            name: manifestation.name,
            manifestationType: manifestation.manifestationType.charAt(0) + manifestation.manifestationType.slice(1).toLowerCase(),
            numOfAvailableSeats: manifestation.numOfAvailableSeats,
            dateTime: manifestation.dateTime,
            regularTicketPrice: parseFloat(manifestation.regularTicketPrice).toFixed(2),
            posterImagePath: manifestation.posterImagePath,
            location: manifestation.street + ', ' + manifestation.city,
            averageGrade: grade
          })
        });
        console.log(manifestations);

        this.manifestations = manifestations

        if (this.manifestations.length === 0) {
          this.hasManifestations = false;
        } else {
          this.hasManifestations = true;
        }

      })
      .catch(function (error) { alert("Error while getting manifestations!") });

  },

  methods: {
    approve(id) {
      axios.get("/api/manifestations/approve?id=" + id)
      .then(response => {
        console.log(response.data)

        var manifestations = [];
        response.data.forEach((manifestation) => {
          var grade;
          if (manifestation.averageGrade == 0) {
            console.log("EVO NULE")
            grade = null;
          } else {
            grade = parseFloat(manifestation.averageGrade).toFixed(2);
          }

          manifestations.push({
            id: manifestation.id,
            name: manifestation.name,
            manifestationType: manifestation.manifestationType.charAt(0) + manifestation.manifestationType.slice(1).toLowerCase(),
            numOfAvailableSeats: manifestation.numOfAvailableSeats,
            dateTime: manifestation.dateTime,
            regularTicketPrice: parseFloat(manifestation.regularTicketPrice).toFixed(2),
            posterImagePath: manifestation.posterImagePath,
            location: manifestation.street + ', ' + manifestation.city,
            averageGrade: grade
          })
        });
        console.log(manifestations);

        this.manifestations = manifestations

      })
      .catch(function (error) { alert("Error while getting manifestations!") });
    }
  }
})