Vue.component('login', {

  data: function () {
    return {
      username: "",
      password: ""
    }
  },

  template:
    `
    <div class="container-fluid">
      <div class="row" style="height: 100vh;">
        <div class="col">
        </div>
        <div class="col-4">
          <div class="text-center">
            <div class="login-form"></div>
            <form>
              <h2 class="text-center">Welcome back!</h2>
              <div class="form-group">
                <input v-model="username" type="text" class="form-control" required="required" placeholder="Username">
              </div>
              <div class="form-group">
                <input v-model="password" type="password" class="form-control" required="required" placeholder="Password">
              </div>
              <div class="form-group">
                <button v-on:click="login" class="btn btn-primary btn-block" type="submit">Log In</button>
              </div>
              <div class="clearfix">
                <router-link :to="'/'" class="float-right">Back to front page</router-link>
              </div>
            </form>
          </div>
        </div>
        <div class="col">
        </div>
      </div>
      <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Error message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="modalMessage"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    `,

  methods: {
    login: function () {
      if (this.username === "" || this.password === "") {
        document.getElementById('modalMessage').innerHTML = 'All of the fields must be filled.';
        $('#loginModal').modal('show');
      } else {
        axios.post("/api/login", {
          "username": this.username,
          "password": this.password
        })
          .then(response => { this.$router.push("/userProfile") })
          .catch(function (error) {
            document.getElementById('modalMessage').innerHTML = 'Invalid username or password.';
            $('#loginModal').modal('show');
          });
      }
    }
  }
})