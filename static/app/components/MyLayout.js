Vue.component("my_layout", {

  data: function() {
    return {
      role: "",
    }
  },

  template: `
  <div class="container-fluid">
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <h2 class="col-sm-3 col-md-2 mr-0">
        <router-link :to="'/'" class="navbar-brand">Ticket Master</router-link>
      </h2>
      <ul class="nav justify-content-end">
        <li class="nav-item">
          <a v-on:click="logout" class="nav-link navigation" href="#">Log out</a>
        </li>
      </ul>
    </nav>
  
  <div class="container-fluid" style="margin-top:10px;">
    <div class="row">
      <nav class="col-md-2 d-none d-md-block sidebar">
        <div class="sidebar-sticky" style="margin-top:50px;">
          <ul class="nav flex-column">

            <li class="nav-item">
              <router-link class="nav-link" to="/userProfile">
                Profile
              </router-link>
            </li>

            <li v-if="role==='SELLER'" class="nav-item">
              <router-link class="nav-link" to="/createManif">
                Create Event
              </router-link>

            </li>

            <li v-if="role==='ADMINISTRATOR'" class="nav-item">
              <router-link class="nav-link" to="/addSeller">
                Add Seller
              </router-link>

            </li>

            <li v-if="role==='SELLER'" class="nav-item">
              <router-link class="nav-link" to="/sellersManifestations">
                My Events
              </router-link>
            </li>

            <li v-if="role==='SELLER'" class="nav-item">
              <router-link class="nav-link" to="/buyers">
                My Buyers
              </router-link>
            </li>

            <li v-if="role==='ADMINISTRATOR'" class="nav-item">
              <router-link class="nav-link" to="/users">
                Registered Users
              </router-link>

            </li>

            <li v-if="role==='ADMINISTRATOR'" class="nav-item">
              <router-link class="nav-link" to="/approveManifestations">
                Approve Event
              </router-link>

            </li>

            <li v-if="role==='ADMINISTRATOR'" class="nav-item">
              <router-link class="nav-link" to="/approveComments">
                Approve Comment
              </router-lik>
            </li>
            
            <li v-if="role==='BUYER'" class="nav-item">
              <router-link class="nav-link" to="/buyTickets">
                Buy Tickets
              </router-link>

            </li>

            <li class="nav-item">
              <router-link class="nav-link" to="/tickets">
                Tickets
              </router-link>
            </li>

            <li class="nav-item">
              <router-link class="nav-link" to="/locations">
                Locations
              </router-link>
            </li>

          </ul>
        </div>
      </nav>

      <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-1" style="position:absolute;left:0;top:0;right:0;bottom:0;padding-top:60px;border-left:2px solid #d6d8d9;">
        <slot></slot>
      </main>

    
    </div>
  </div>


</div>
  `,

  mounted() {
    this.role = localStorage.getItem("role");
  },

  methods: {
    logout : function () {
        axios
        .get("api/users/logout")
        .then(response  => {
            this.$router.push("/frontPage");
        })
        .catch(function(error){alert(error.response.data);
        })
    },

}


});