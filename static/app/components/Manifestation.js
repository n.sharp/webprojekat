Vue.component('manifestation', {
  data: function () {
    return {
      manifestation: {},
      comments: [],
      role: ''
    };
  },
  template:
  `
  <div class="container-fluid">
  <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <h2 class="col-sm-3 col-md-2 mr-0">
        <router-link :to="'/'" class="navbar-brand">Ticket Master</router-link>
      </h2>
      <div v-if="role == null">
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <router-link :to="'/login'" class="nav-link navigation">Log In</router-link>
          </li>
          <li class="nav-item">
            <router-link :to="'/registration'" class="nav-link navigation">Registration</router-link>
          </li>
        </ul>
      </div>
      <div v-else>
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <router-link :to="'/userProfile'" class="nav-link navigation">Dashboard</router-link>
          </li>
        </ul>
      </div>
    </nav>
  <div class="row" style="height: 100vh;">
    <div class="col">
    </div>

    <div class="col-6">
      <div class="text-center">
        <div class="login-form"></div>
        <form>
          <h1 class="text-center">{{manifestation.name}}</h1>

          <div class="row d-flex justify-content-center">
            <img :src="manifestation.posterImagePath" width="50%" height="50%" alt="Picture" />
          </div>

          <h3>{{manifestation.dateTime}}</h3>

          <div class="form-group row">
            <div class="col-6">
              <label>Regular Ticket Price</label>
              <input readonly v-model="manifestation.regularTicketPrice" type="text" class="form-control" readonly>
            </div>

            <div class="col-6">
              <label>Type of event</label>
              <input readonly v-model="manifestation.manifestationType" type="text" class="form-control" readonly>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-6">
              <label>Number of tickets available</label>
              <input readonly v-model="manifestation.numOfTicketsLeft" type="text" class="form-control" readonly>
            </div>

            <div class="col-6">
              <label>Number of seats</label>
              <input readonly v-model="manifestation.numOfAvailableSeats" type="text" class="form-control" readonly>
            </div>
          </div>

          <div class="form-group row">
            <div class="col-6">
              <label>Event active</label>
              <input readonly v-model="manifestation.isActive ? 'Yes' : 'No'" type="text" class="form-control" readonly>
            </div>

            <div class="col-6">
              <label>Lokacija</label>
              <input readonly v-model="manifestation.street + ', ' + manifestation.city" type="text" class="form-control" readonly>
            </div>
          </div>

        </form>
      </div>
    </div>

    <div class="col-4">
      <div class="text-center" style="margin-top:120px;">
        <h4>Comments</h4>
        <hr/>
        <div class="container" v-for="comment in comments">
          <div v-if="(role == 'BUYER' || role == null) && comment.commentStatus == 'APPROVED'">
            <div class="row">
              <div class="col-sm">
                <b class="mx-4">{{comment.author}}</b>
              </div>
              <div class="col-sm">
              </div>
              <div class="col-sm">
                <span v-for="star in comment.grade" class="fa fa-star" style="color:orange;"></span>
              </div>
            </div>
            <p style="padding-top:10px; padding-bottom:20px;">{{comment.text}}</p>
            <hr/>
          </div>
          <div v-else-if="(role == 'ADMINISTRATOR' || role == 'SELLER') && comment.commentStatus != 'CREATED'">
            <div class="row">
              <div class="col-sm">
                <b class="mx-4">{{comment.author}}</b>
              </div>
              <div class="col-sm">
              </div>
              <div class="col-sm">
                <span v-for="star in comment.grade" class="fa fa-star" style="color:orange;"></span>
              </div>
            </div>
            <p style="padding-top:10px;">{{comment.text}}</p>
            <a v-if="role == 'ADMINISTRATOR'" href="#" v-on:click="deleteComment(comment)">Delete</a>
            <hr style="padding-top:20px;"/>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
  
  `
  ,

  mounted() {
    var id = this.$route.params.id;
    axios.get('/api/manifestations/manifestation/' + id).then(response => (this.manifestation = response.data));

    axios.get('api/comments/manifestationComments?id=' + id).then(response => this.comments = response.data);

    this.role = localStorage.getItem('role');
  },

  methods: {
    deleteComment(comment) {
      axios.post('api/comments/deleteComment', comment).then(response => (this.comments = response.data));
    }
  }
});