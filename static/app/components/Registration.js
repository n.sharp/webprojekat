Vue.component("registration", {

  data: function () {
    return {
      username: "",
      password1: "",
      password2: "",
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      gender: ""
    }
  },

  template:
    `
  <div class="container-fluid">
  <div class="row" style="height: 100vh;">
    <div class="col">
    </div>

    <div class="col-4">
      <div class="text-center">
        <div class="login-form"></div>
        <form>
          <h2 class="text-center">Join us and become a Ticket Master!</h2>

          <div class="form-group">
            <input v-model="username" type="text" class="form-control" placeholder="Username">
          </div>

          <div class="form-group">
            <input v-model="password1" type="password" class="form-control" placeholder="Password">
          </div>

          <div class="form-group">
            <input v-model="password2" type="password" class="form-control" placeholder="Repeat Password">
          </div>

          <div class="form-group">
            <input v-model="firstName" type="text" class="form-control" placeholder="First Name">
          </div>

          <div class="form-group">
            <input v-model="lastName" type="text" class="form-control" placeholder="Last Name">
          </div>

          <div class="form-group">
            <input v-model="dateOfBirth" class="form-control" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date of birth">
          </div>

          <div class="form-group">
            <select class="form-control" id="gender" v-model="gender">
              <option value="" disabled selected>Gender</option>
              <option value="MALE">Male</option>
              <option value="FEMALE">Female</option>
            </select>
          </div>

          <div class="form-group">
            <button v-on:click="register" class="btn btn-primary btn-block" type="submit">Register</button>
          </div>

          <div class="clearfix">
            <router-link :to="'/'" class="float-right">Cancel</router-link>
          </div>

        </form>
      </div>
    </div>

    <div class="col">
    </div>

    <div class="modal fade" id="registrationModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="modalTitle">Error message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p id="modalMessage"></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
  `,

  methods: {

    register: function () {
      if (this.username == '' || this.password1 == '' || this.password2 == '' || this.firstName == '' || this.lastName == '' || this.dateOfBirth == '' || this.gender == '') {
        document.getElementById('modalMessage').innerHTML = "All of the fields must be filled.";
      } else {
        if (this.password1 !== this.password2) {
          document.getElementById('modalMessage').innerHTML = "Passwords doesn't match";
        } else {
          axios.post("/api/register", {
            "username": this.username,
            "password": this.password1,
            "firstName": this.firstName,
            "lastName": this.lastName,
            "gender": this.gender,
            "dateOfBirth": this.dateOfBirth,
            "role": "BUYER",
            "isDeleted": false,
            "tickets": [],
            "points": 0,
            "buyerType": "REGULAR",
            "isBlocked": false
          })
            .then(response => {
              document.getElementById('modalMessage').innerHTML = "Registration successful!";
              this.$router.push("/")
            }
            ).catch(function (error) {
              document.getElementById('modalMessage').innerHTML = String(error.response.data);
            });
        }
      }

      $('#registrationModal').modal('show');
    }
  }
})