Vue.component('users', {
  data:
    function () {
      return {
        users: [],
        allUsers: [],
        hasUsers: false,
        searchFirstName: "",
        searchLastName: "",
        searchUserName: "",
        sorted: false,
      }
    },

  template: `
  <my_layout>
  <div class="container-fluid">
    <div class="form-inline row justify-content-center" style="margin-top:5%; margin-bottom:1%;">
      <input v-model="searchFirstName" class="form-control search-item" type="text" placeholder="First Name" />
      <input v-model="searchLastName" class="form-control search-item" type="text" placeholder="Last Name" />
      <input v-model="searchUserName" class="form-control search-item" type="text" placeholder="Username" />

      <button v-on:click="search()" class="btn btn-primary search-item" type="submit">Submit</button>
    </div>

    <div class="text-center center">
      <div class="table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Username</th>
              <th>Password</th>
              <th>First name</th>
              <th>Last name</th>
              <th>Gender</th>
              <th>Date of birth</th>
              <th>Role</th>
              <th>Type of Buyer</th>
              <th>Buyer's points</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="user in users">
              <td class="sortable" v-on:click="sort('username')">{{user.username}}</td>
              <td>{{user.password}}</td>
              <td class="sortable" v-on:click="sort('firstName')">{{user.firstName}}</td>
              <td class="sortable" v-on:click="sort('lastName')">{{user.lastName}}</td>
              <td>{{user.gender}}</td>
              <td>{{user.dateOfBirth}}</td>
              <td>{{user.role}}</td>
              <td>{{user.buyerType}}</td>
              <td class="sortable" v-on:click="sort('points')">{{user.points}}</td>
              <td>
                <a v-if="user.role !== 'Administrator'" href="#" v-on:click="deleteUser(user.username)">Delete</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      
      

      <div class="form-inline row justify-content-center" style="margin-top:10px; margin-bottom:80px;">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="role_buyer" v-on:click="filter" checked>
          <label class="form-check-label">Buyer</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="role_seller" v-on:click="filter" checked>
          <label class="form-check-label">Seller</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="role_admin" v-on:click="filter" checked>
          <label class="form-check-label">Administrator</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="type_gold" v-on:click="filter" checked>
          <label class="form-check-label">Golden Buyer</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="type_silver" v-on:click="filter" checked>
          <label class="form-check-label">Silver Buyer</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="type_bronze" v-on:click="filter">
          <label class="form-check-label">Bronze Buyer</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="type_regular" v-on:click="filter">
          <label class="form-check-label">Regular Buyer</label>
        </div>
      </div>
    </div>
  </div>
</my_layout>
    `,

  methods: {
    deleteUser(username) {
      axios.get('/api/users/deleteUser?username=' + username).then(response => {
        var users = [];
        response.data.forEach((user) => {
          var dateFormatted = user.dateOfBirth.day.toString().padStart(2, '0') + '.' + user.dateOfBirth.month.toString().padStart(2, '0') + '.' + user.dateOfBirth.year + '.';

          users.push({
            username: user.username,
            password: user.password,
            firstName: user.firstName,
            lastName: user.lastName,
            gender: user.gender.charAt(0) + user.gender.slice(1).toLowerCase(),
            dateOfBirth: dateFormatted,
            role: user.role.charAt(0) + user.role.slice(1).toLowerCase(),

            buyerType: user.buyerType.charAt(0) + user.buyerType.slice(1).toLowerCase(),
            points: user.points
          })
        });
        this.users = users;
        this.allUsers = users;

        if (this.users.length === 0) {
          this.hasUsers = false;
        } else {
          this.hasUsers = true;
        }
      })
    },

    search() {
      axios.get('/api/users/search?firstName='+ this.searchFirstName + 
      '&lastName='+ this.searchLastName + '&username=' + this.searchUserName)
      .then(response => {
        var users = [];

        response.data.forEach((user) => {
          users.push({
            username: user.username,
            password: user.password,
            firstName: user.firstName,
            lastName: user.lastName,
            gender: user.gender.charAt(0) + user.gender.slice(1).toLowerCase(),
            dateOfBirth: dateFormatted,
            role: user.role.charAt(0) + user.role.slice(1).toLowerCase(),

            buyerType: user.buyerType.charAt(0) + user.buyerType.slice(1).toLowerCase(),
            points: user.points
          })
        });

        this.users = users;
        this.allUsers = users;

        if (this.users.length === 0) {
          this.hasUsers = false;
        } else {
          this.hasUsers = true;
        }

        document.getElementById('role_buyer').checked = true;
        document.getElementById('role_seller').checked = true;
        document.getElementById('role_admin').checked = true;
        document.getElementById('type_gold').checked = true;
        document.getElementById('type_silver').checked = true;
        document.getElementById('type_bronze').checked = true;
        document.getElementById('type_regular').checked = true;
      })

    },


    
    sort(sortProp) {
      if(sortProp=== "username"){
        if(this.sorted){
          this.users.sort((m1, m2) => m1.username.toLowerCase() > m2.username.toLowerCase() ? -1 : 1);
        }else{
          this.users.sort((m1, m2) => m1.username.toLowerCase() > m2.username.toLowerCase() ? 1 : 1);
        }
      }else if (sortProp === "firstname"){
        if(this.sorted){
          this.users.sort((m1, m2) => m1.firstName.toLowerCase() > m2.firstName.toLowerCase() ? -1 : 1);
        }else{
          this.users.sort((m1, m2) => m1.firstName.toLowerCase() > m2.firstName.toLowerCase() ? 1 : 1);
        }
      }else if (sortProp === "lastname"){
        if(this.sorted){
          this.users.sort((m1, m2) => m1.lastName.toLowerCase() > m2.lastName.toLowerCase() ? -1 : 1);
        }else{
          this.users.sort((m1, m2) => m1.lastName.toLowerCase() > m2.lastName.toLowerCase() ? 1 : 1);
        }
      }else if(sortProp==="points"){
        if(this.sorted){
          this.users.sort((m1, m2) => m1.points > m2.points ? -1 : 1);
        }else{
          this.users.sort((m1, m2) => m1.points > m2.points ? 1 : 1);
        }
      }
    },

    filter(){
      var filterType = [];
      if (document.getElementById('role_buyer').checked)
      filteredType = filteredType.concat(this.allUsers.filter(u => u.role === 'Buyer'));

      if (document.getElementById('role_seller').checked)
        filteredType = filteredType.concat(this.allUsers.filter(u => u.role === 'Seller'));

      if (document.getElementById('role_admin').checked)
        filteredType = filteredType.concat(this.allUsers.filter(u => u.role === 'Administrator'));

      if (document.getElementById('type_gold').checked)
        filteredType = filteredType.concat(this.allUsers.filter(u => u.buyerType === 'Gold'));

      if (document.getElementById('type_silver').checked)
        filteredType = filteredType.concat(this.allUsers.filter(u => u.buyerType === 'Silver'));

      if (document.getElementById('type_bronze').checked)
        filteredType = filteredType.concat(this.allUsers.filter(u => u.buyerType === 'Bronze'));

      if (document.getElementById('type_regular').checked)

        filteredType = filteredType.concat(this.allUsers.filter(u => u.buyerType === 'Regular'));     

      this.users = filterType.filter(val => val);

      if (this.users.length === 0) {
        this.hasUsers = false;
      } else {
        this.hasUsers = true;
      }
    },


    



  },

  mounted() {
    console.log("MOUNTED")
    axios.get('/api/users/users').then(response => {

      var users = [];
      response.data.forEach((user) => {
        var dateFormatted = user.dateOfBirth.day.toString().padStart(2, '0') + '.' + user.dateOfBirth.month.toString().padStart(2, '0') + '.' + user.dateOfBirth.year + '.';

        //var buyerType = user.

        users.push({
          username: user.username,
          password: user.password,
          firstName: user.firstName,
          lastName: user.lastName,
          gender: user.gender.charAt(0) + user.gender.slice(1).toLowerCase(),
          dateOfBirth: dateFormatted,
          role: user.role.charAt(0) + user.role.slice(1).toLowerCase(),

          buyerType: "",
          //buyerType: user.buyerType.charAt(0) + user.buyerType.slice(1).toLowerCase(),
          points: user.points
        })
      });

      this.users = users;
      this.allUsers = users;
      console.log("DUZINA: "+ this.users.length)
      if (this.users.length === 0) {
        this.hasUsers = false;
      } else {
        this.hasUsers = true;
      }

    }).catch(function (error) {
      alert("error when getting users")
      //document.getElementById('errorMessage').innerHTML = error.response;
    });
  }
})