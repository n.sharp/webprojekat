Vue.component("buyers", {
  data: function () {
    return{
      buyers: [],
      username: "",
    }
  },

  template:`
  <my_layout>
  <body>
  <div class="row container-fluid">
  <div class="table-responsive">
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Username</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Gender</th>
        <th>Type of Buyer</th>
        <th>Buyer's points</th>
      </tr>
    </thead>
    <tbody>
      <tr v-for="user in buyers">
        <td>{{user.username}}</td>
        <td>{{user.firstName}}</td>
        <td>{{user.lastName}}</td>
        <td>{{user.gender}}</td>
        <td>{{user.buyerType}}</td>
        <td>{{user.points}}</td>

      </tr>
    </tbody>
  </table>
</div>
</div>
</body>
</my_layout>
  `,

  mounted() {
    this.username = localStorage.getItem("username");
    axios.get('/api/users/buyersBySeller?username=' + this.username).then(response => {

      var buyers = [];
      response.data.forEach((user) => {
        buyers.push({
          username: user.username,
          firstName: user.firstName,
          lastName: user.lastName,
          gender: user.gender.charAt(0) + user.gender.slice(1).toLowerCase(),
          buyerType: user.buyerType.charAt(0) + user.buyerType.slice(1).toLowerCase(),
          points: user.points
        })

      });

      this.buyers = buyers;
    }).catch(function (error) {
      alert("error when getting buyers")
    });
  },


})
