Vue.component("editManifestation", {

  data: function () {
    return {
      manifestation: {},
      locId: -1
    }
  },


  template: `
  <my_layout>
  <div class="container-fluid">
    <div class="row" style="height: 100vh;">
      <div class="col">
      </div>

      <div class="col-6">
        <div class="text-center">
          <div class="login-form"></div>
          <form id="theForm">
            <h2>Edit Event</h2>

            <div class="form-group row">
            <div class="col-12">
              <input id="dateEl" class="form-control" type="text" onfocus="(this.type='date')"
                onblur="(this.type='text')" placeholder="Date" v-model="manifestation.date">
                </div>
            </div>

            <div class="form-group row">
              <div class="col-6">
                <label>Title</label>
                <input v-model="manifestation.name" type="text" class="form-control">
              </div>

              <div class="col-6">
                <label>Time of event</label>
                <input v-model="manifestation.time" type="text" class="form-control">
              </div>
            </div>

            <div class="form-group row">

              <div class="col-6">
                <label>Regular Ticket Price</label>
                <input v-model="manifestation.regularTicketPrice" type="number" class="form-control">
              </div>

              <div class="col-6">
                <label>Type of event</label>
                <select class="form-control" v-model="manifestation.manifestationType">
                  <option value="CONCERT">Concert</option>
                  <option value="FESTIVAL">Festival</option>
                  <option value="SHOW">Show</option>
                  <option value="OPERA">Opera</option>
                  <option value="SPORT">Sport</option>
                </select>
              </div>


            </div>

            <div class="form-group row">

              <div class="col-6">
                <label>Number of seats</label>
                <input v-model="manifestation.numOfAvailableSeats" type="number" class="form-control">
              </div>

              <div class="col-6">
                <label>Path to Image Poster(from static folder)</label>
                <input v-model="manifestation.posterImagePath" type="text" class="form-control">
              </div>

            </div>

            <div class="form-group row">
              <div class="col-6">
                <label>Location</label>

                <input v-model="manifestation.longitude" type="text" class="form-control" placeholder="Longitude">
                <input v-model="manifestation.latitude" type="text" class="form-control" placeholder="Latitude">
              </div>

              <div class="col-6">
                <input v-model="manifestation.street" type="text" class="form-control" placeholder="Address">
                <input v-model="manifestation.city" type="text" class="form-control" placeholder="City">
                <input v-model="manifestation.postalNumber" type="text" class="form-control" placeholder="Postal Number">
              </div>

            </div>
          </form>
        </div>

        <div class="form-group">
          <button v-on:click="editEvent" class="btn btn-primary btn-block" type="submit">Save</button>
        </div>
        </form>
      </div>

      <div class="col">
      </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalTitle">Error message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="modalMessage"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

   

  </div>

  </div>
</my_layout>
  `,

  mounted() {
    var id = this.$route.params.id;
    axios.get('/api/manifestations/manifestation/' + id).then(response => {
      this.manifestation = response.data;
      var dateTimeParts = this.manifestation.dateTime.split(' ');
      this.manifestation.date = dateTimeParts[0];
      this.manifestation.time = dateTimeParts[1];
    });
  },

  methods: {
    editEvent: function () {
      if (this.date == '' || this.title == '' || this.time == '' || this.regularTicketPrice == 0 || this.type == '' || this.numOfAvailableSeats == '' || this.pathToImage == '' || this.longitude == '' || this.latitude == '' || this.street == '' || this.city == '' || this.postalNumber == '') {
        document.getElementById('modalMessage').innerHTML = 'All of the fields must be filled.';
        $('#editModal').modal('show');
        return;
      }
      
      axios.post("/api/locations/getIdOfLocationOrCreate", {
        id: 0,
        longitude: this.manifestation.longitude,
        latitude: this.manifestation.latitude,
        street: this.manifestation.street,
        city: this.manifestation.city,
        postalNumber: this.manifestation.postalNumber,
        isDeleted: false
      })
        .then(response => {
          this.locId = response.data;

          var input = document.getElementById('dateEl').value;
          var d = new Date(input);
          if (!!d.valueOf()) {
            year = d.getFullYear();
            month = d.getMonth() + 1;
            day = d.getDate();

            dateString = day.toString().padStart(2, '0') + "." + month.toString().padStart(2, '0') + "." + year + ".";
            timeString = this.manifestation.time;
            dateTimeString = dateString + " " + timeString;
          }

          axios.post("/api/manifestations/editManifestation", {
            id: this.manifestation.id,
            name: this.manifestation.name,
            manifestationType: this.manifestation.manifestationType,
            numOfAvailableSeats: parseInt(this.manifestation.numOfAvailableSeats),
            dateTime: dateTimeString,
            regularTicketPrice: parseInt(this.manifestation.regularTicketPrice),
            isActive: this.manifestation.isActive,
            locationId: this.locId,
            posterImagePath: this.manifestation.pathToImage,
            isDeleted: this.manifestation.isDeleted
          })
            .then(response2 => {
              document.getElementById('modalTitle').innerHTML = 'Successful';
              document.getElementById('modalMessage').innerHTML = 'You edited manifestation successfully!';
              $('#editModal').modal('show');
              
              this.locId = -1;
            })
            .catch(function (error) {
              document.getElementById('modalMessage').innerHTML = String(error.response.data);
              $('#editModal').modal('show');
            });
        })
        .catch(function (error) {
          document.getElementById('modalMessage').innerHTML = String(error.response.data);
          $('#editModal').modal('show');
        });
    },
  }
});