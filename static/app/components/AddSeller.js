Vue.component('addSeller', {
  data:
    function () {
      return {
        seller: {
          username: '',
          password: '',
          firstName: '',
          lastName: '',
          gender: '',
          dateOfBirth: '',
          role: 'SELLER',
          isDeleted: false,
          manifestations: []
        }
      }
    },

  template: `
  <my_layout>
    <body>
      <div class="container-fluid">
        <div class="row" style="height: 100vh;">
          <div class="col">
          </div>

          <div class="col-4">
            <div class="text-center">
              <div class="login-form" id="sellerForm"></div>
                <form>
                  <h2 class="text-center">Add new seller</h2>
                  <div class="form-group">
                    <input v-model="seller.username" type="text" class="form-control" placeholder="Username">
                  </div>
                  <div class="form-group">
                    <input v-model="seller.password" type="password" class="form-control" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <input v-model="seller.firstName" type="text" class="form-control" placeholder="First Name">
                  </div>
                  <div class="form-group">
                    <input v-model="seller.lastName" type="text" class="form-control" placeholder="Last Name">
                  </div>
                  <div class="form-group">
                    <input v-model="seller.dateOfBirth" class="form-control" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date of birth">
                  </div>
                  <div class="form-group">
                    <select class="form-control" id="gender" v-model="seller.gender">
                      <option value="" disabled selected>Gender</option>
                      <option value="MALE">Male</option>
                      <option value="FEMALE">Female</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <button v-on:click="add" class="btn btn-primary btn-block" type="submit">Save</button>
                  </div>
                  <div class="clearfix">
                    <a href="/" class="float-right">Cancel</a>
                  </div>
                </form>
              </div>
            </div>
          <div class="col">
          </div>
        </div>
      </div>
      <div class="modal fade" id="sellerModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalTitle">Error message</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p id="modalMessage"></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </body>
    </my_layout>
  `,

  methods: {
    add() {
      if (this.seller.username == '' || this.seller.password == '' || this.seller.firstName == '' || this.seller.lastName == '' || this.seller.dateOfBirth == '' || this.seller.gender == '') {
        document.getElementById('modalMessage').innerHTML = 'All of the fields must be filled.';
      } else {
        axios.post('/api/users/addSeller', this.seller).then(response => {
          document.getElementById('modalTitle').innerHTML = 'Successful';
          document.getElementById('modalMessage').innerHTML = 'You added new seller successfully!';
          
          this.seller.username = '';
          this.seller.password = '';
          this.seller.firstName = '';
          this.seller.lastName = '';
          this.seller.gender = '';
          this.seller.dateOfBirth = '';
        }).catch(function (error) {
          document.getElementById('modalMessage').innerHTML = String(error.response.data);
        });
      }

      $('#sellerModal').modal('show');
    }
  },

  mounted() {
  }
})