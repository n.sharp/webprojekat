Vue.component('tickets', {
  data:
    function () {
      return {
        allTickets: [],
        tickets: [],
        allowed: false,
        role: '',
        searchManif: '',
        priceFrom: '',
        priceTo: '',
        dateFrom: '',
        dateTo: '',
        vip: true,
        regular: true,
        fanpit: true,
        sorted: false,
        hasTickets: false
      }
    },

  template: `
  <my_layout>
    <body class="text-center center">
      <div v-if="allowed">
        <div class="form-inline d-flex justify-content-center" v-if="role === 'BUYER'">
          <input v-model="searchManif" class="form-control search-item" type="text" placeholder="Manifestation"/>
          <input v-model="priceFrom" class="form-control search-item" type="number" min="1" placeholder="Price from..."/>
          <input v-model="priceTo" class="form-control search-item" type="number" min="1" placeholder="Price to..."/>
          <input v-model="dateFrom" class="form-control search-item" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date from..."/>
          <input v-model="dateTo" class="form-control search-item" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date to..."/>
          <button v-on:click="search" class="btn btn-primary search-item" type="submit">Submit</button>
        </div>
        <br>
        <div class="table-responsive" v-if="hasTickets">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th class="sortable" v-on:click="sort('manif')">Manifestation</th>
                <th class="sortable" v-on:click="sort('date')">Date and time</th>
                <th class="sortable" v-on:click="sort('price')">Price</th>
                <th>Buyer</th>
                <th>Status</th>
                <th>Type</th>
                <th v-if="role === 'ADMINISTRATOR'">Delete</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="ticket in tickets">
                <td>{{ticket.id}}</td>
                <td>{{ticket.manifestation}}</td>
                <td>{{ticket.manifestationDateTime}}</td>
                <td>{{ticket.price}}</td>
                <td>{{ticket.buyer}}</td>
                <td>{{ticket.ticketStatus}}</td>
                <td>{{ticket.ticketType}}</td>
                <td>
                  <a href="#" v-if="role === 'ADMINISTRATOR'" v-on:click="deleteTicket(ticket.id)">Delete</a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div v-else>
          <h4>There is no tickets</h4>
        </div>
          <br>
          <div v-if="role === 'BUYER'">
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="vip" v-on:click="filter" checked>
              <label class="form-check-label">Vip</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="regular" v-on:click="filter" checked>
              <label class="form-check-label">Regular</label>
            </div>
            <div class="form-check form-check-inline">
              <input class="form-check-input" type="checkbox" id="fanpit" v-on:click="filter" checked>
              <label class="form-check-label">Fanpit</label>
            </div>
          </div>
      </div>
      <div v-else>
        <h4 id="errorMessage"></h4>
      </div>
    </body>
    </my_layout>
    `,

  methods: {
    loadTickets(dataTickets) {
      var tickets = [];

      dataTickets.forEach((ticket) => {
        var dateFormatted = ticket.dateTime.date.day.toString().padStart(2, '0') + '.' + ticket.dateTime.date.month.toString().padStart(2, '0') + '.' + ticket.dateTime.date.year + '.';
        var timeFormatted = ticket.dateTime.time.hour.toString().padStart(2, '0') + ':' + ticket.dateTime.time.minute.toString().padStart(2, '0');

        tickets.push({
          id: ticket.id,
          manifestation: ticket.manifestation,
          manifestationDateTime: dateFormatted + ' ' + timeFormatted,
          price: ticket.price.toFixed(2),
          buyer: ticket.buyer,
          ticketStatus: ticket.ticketStatus.charAt(0) + ticket.ticketStatus.slice(1).toLowerCase(),
          ticketType: ticket.ticketType.charAt(0) + ticket.ticketType.slice(1).toLowerCase()
        })
      });

      this.allTickets = tickets;
      this.tickets = tickets;
      this.checkLength();
    },

    search() {
      axios.get('/api/tickets/searchTickets?manifestation=' + this.searchManif + '&priceFrom=' + this.priceFrom + '&priceTo=' + this.priceTo + '&dateFrom=' + this.dateFrom + '&dateTo=' + this.dateTo).then(response => {
        this.loadTickets();

        document.getElementById('vip').checked = true;
        document.getElementById('regular').checked = true;
        document.getElementById('fanpit').checked = true;
      });
    },

    stringToDateTime(dtString) {
      dateParts = dtString.split(' ')[0].split('.');
      time = dtString.split(' ')[1];
      return new Date(dateParts[1] + '/' + dateParts[0] + '/' + dateParts[2] + ' ' + time);
    },

    sort(sortProp) {
      if (sortProp === 'manif') {
        if (this.sorted)
          this.tickets.sort((t1, t2) => t1.manifestation > t2.manifestation ? -1 : 1);
        else
          this.tickets.sort((t1, t2) => t1.manifestation > t2.manifestation ? 1 : -1);
      } else if (sortProp === 'date') {
        if (this.sorted)
          this.tickets.sort((t1, t2) => this.stringToDateTime(t1.manifestationDateTime) > this.stringToDateTime(t2.manifestationDateTime) ? -1 : 1);
        else
          this.tickets.sort((t1, t2) => this.stringToDateTime(t1.manifestationDateTime) > this.stringToDateTime(t2.manifestationDateTime) ? 1 : -1);
      } else {
        if (this.sorted)
          this.tickets.sort((t1, t2) => t1.price - t2.price);
        else
          this.tickets.sort((t1, t2) => t2.price - t1.price);
      }

      this.sorted = !this.sorted;
    },

    filter() {
      var filtered = [];

      if (document.getElementById('vip').checked)
        filtered = filtered.concat(this.allTickets.filter(ticket => ticket.ticketType === 'Vip'));
      if (document.getElementById('regular').checked)
        filtered = filtered.concat(this.allTickets.filter(ticket => ticket.ticketType === 'Regular'));
      if (document.getElementById('fanpit').checked)
        filtered = filtered.concat(this.allTickets.filter(ticket => ticket.ticketType === 'Fanpit'));

      this.tickets = filtered;
      this.checkLength();
    },

    checkLength() {
      if (this.tickets.length === 0) {
        this.hasTickets = false;
      } else {
        this.hasTickets = true;
      }
    },

    deleteTicket(ticketId) {
      axios.get('/api/tickets/deleteTicket?id=' + ticketId).then(response => this.loadTickets(response.data));
    }
  },

  mounted() {
    this.role = localStorage.getItem('role');

    axios.get('/api/tickets/tickets').then(response => {
      this.loadTickets(response.data);
      this.allowed = true;
    }).catch(function (error) {
      this.allowed = false;
      document.getElementById('errorMessage').innerHTML = error.response.data;
    });
  }
})