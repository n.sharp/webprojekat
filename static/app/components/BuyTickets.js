Vue.component("buyTickets", {
  data: function () {
    return {
      manifestations: [],
      numVip: 0,
      numFanpit: 0,
      numReg: 0,
      totalPrice: 0,
      showPrice: false,
      manName: "",
      currentRegPrice: 0,
      calculatedPrice: 0,
      currentManifestationId: 0,
      username: ""
    }
  }

  ,
  template: `
<my_layout>
<div class="text-center mx-5 row" >
  <div class="table-responsive">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Image</th>
          <th class="sortable" v-on:click="sort('name')">Name</th>
          <th>Type</th>
          <th class="sortable" v-on:click="sort('dateTime')">Date and time</th>
          <th class="sortable" v-on:click="sort('price')">Regular ticket price</th>
          <th class="sortable" v-on:click="sort('location')">Location</th>
          <th>Average grade</th>
          <th>Buy</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="manifestation in manifestations">
          <td width="15%">
            <img :src="manifestation.posterImagePath" width="100%" alt="Picture" style="display:block;" />
          </td>
          <td>{{manifestation.name}}</td>
          <td>{{manifestation.manifestationType}}</td>
          <td>{{manifestation.dateTime}}</td>
          <td>{{manifestation.regularTicketPrice}}</td>
          <td>{{manifestation.location}}</td>
          <td>{{manifestation.averageGrade}}</td>
          <td>
            <a href="#" v-on:click="showBuyModal(manifestation)">Buy</a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>


  <div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">

      <div class="modal-content">

        <div class="modal-header">
          <h5 class="modal-title" id="modalTitle">Fill out form {{manName}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>


        <div class="modal-body">

          <div class="login-form">
            <form>
              <div class="form-group">
                <label>Number of VIP tickets</label>
                <input v-model="numVip" type="number" class="form-control">
              </div>

              <div class="form-group">
              <label>Number of Fanpit tickets</label>
                <input v-model="numFanpit" type="number" class="form-control">
              </div>

              <div class="form-group">
              <label>Number of Regular tickets</label>
                <input v-model="numReg" type="number" class="form-control">
              </div>

              <div class="form-group">
                <button v-on:click="modalShowPriceAndBuy" class="btn btn-primary btn-block" type="submit" >Buy</button>
              </div>
            </form>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</my_layout>
`,

  mounted() {
    this.username = localStorage.getItem("username");

    axios.get("/api/manifestations/active")
      .then(response => {
        var manifestations = [];
        response.data.forEach((manifestation) => {
          var grade;
          if (manifestation.averageGrade == 0) {
            grade = null;
          } else {
            grade = parseFloat(manifestation.averageGrade).toFixed(2);
          }

          manifestations.push({
            id: manifestation.id,
            name: manifestation.name,
            manifestationType: manifestation.manifestationType.charAt(0) + manifestation.manifestationType.slice(1).toLowerCase(),
            numOfAvailableSeats: manifestation.numOfAvailableSeats,
            dateTime: manifestation.dateTime,
            regularTicketPrice: parseFloat(manifestation.regularTicketPrice).toFixed(2),
            posterImagePath: manifestation.posterImagePath,
            location: manifestation.street + ', ' + manifestation.city,
            averageGrade: grade
          })
        });

        this.manifestations = manifestations;
      });
  },

  methods: {
    logout: function () {
      axios
        .get("api/users/logout")
        .then(response => {
          this.$router.push("/");
        })
        .catch(function (error) { alert(error.response.data); })
    },

    modalShowPriceAndBuy() {
      console.log("Now we calc")
      this.calculateTotal();
      console.log("NOW WE BUY")
      this.buyTickets();
      $("#buyModal").modal('hide');
      this.$router.push("/buyTickets");

    },
    showBuyModal(manifestation) {
      console.log("EVO STA SAM DOBILA")
      console.log(manifestation);
      this.currentRegPrice = manifestation.regularTicketPrice;
      this.currentManifestationId = manifestation.id;
      this.manName = manifestation.name;
      $('#buyModal').modal('show');
    },
  
  
  
    calculateTotal() {
      axios
        .get("/api/tickets/calculate?username=" + this.username + "&basePrice=" + this.currentRegPrice + "&numOfVIP=" +
          this.numVip + "&numOfFanpit=" + this.numFanpit + "&numOfRegular=" + this.numReg)
        .then(response => {
          console.log(response.data);
          this.calculatedPrice = response.data;
          alert("Total price: " + this.calculatedPrice);
        })
        .catch(function (error) { alert(error.response.data); })
    },
  
  
    buyTickets() {
      axios
        .get("/api/tickets/buyTickets?manId=" + this.currentManifestationId + "&username=" + this.username + "&numVIP="
          + this.numVip + "&numFanpit=" + this.numFanpit + "&numReg=" + this.numReg)
  
        .then(response => {
          alert("You've succeeded!")
        })
        .catch(function (error) { alert(error.response.data); })
    },

  }

})
