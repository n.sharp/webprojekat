Vue.component('frontpage', {
  data: function () {
    return {
      manifestations: [],
      allManifestations: [],
      searchName: '',
      dateFrom: '',
      dateTo: '',
      searchLocation: '',
      priceFrom: '',
      priceTo: '',
      sorted: false,
      hasManifestations: false,
      role: ''
    };
  },

  template:
    `
  <div class="container-fluid">
    <nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <h2 class="col-sm-3 col-md-2 mr-0">
        <router-link :to="'/'" class="navbar-brand">Ticket Master</router-link>
      </h2>
      <div v-if="role == 'ADMINISTRATOR' || role == 'BUYER' || role == 'SELLER'">
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <router-link :to="'/userProfile'" class="nav-link navigation">Dashboard</router-link>
          </li>
        </ul>
      </div>
      <div v-else>
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <router-link :to="'/login'" class="nav-link navigation">Log In</router-link>
          </li>
          <li class="nav-item">
            <router-link :to="'/registration'" class="nav-link navigation">Registration</router-link>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="form-inline row justify-content-center" style="margin-top:7%; margin-bottom:1%">
        <input v-model="searchName" class="form-control search-item" type="text" placeholder="Manifestation"/>
        <input v-model="dateFrom" class="form-control search-item" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date from..."/>
        <input v-model="dateTo" class="form-control search-item" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date to..."/>
        <input v-model="searchLocation" class="form-control search-item" type="text" placeholder="Location"/>
        <input v-model="priceFrom" class="form-control search-item" type="number" min="1" placeholder="Price from..."/>
        <input v-model="priceTo" class="form-control search-item" type="number" min="1" placeholder="Price to..."/>
        <button v-on:click="search" class="btn btn-primary search-item" type="submit">Submit</button>
      </div> 
      <div class="text-center mx-5 row" v-if="hasManifestations">
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Image</th>
                <th class="sortable" v-on:click="sort('name')">Name</th>
                <th>Type</th>
                <th class="sortable" v-on:click="sort('dateTime')">Date and time</th>
                <th class="sortable" v-on:click="sort('price')">Regular ticket price</th>
                <th class="sortable" v-on:click="sort('location')">Location</th>
                <th>Average grade</th>
                <th>View</th>
                <th v-if="role === 'ADMINISTRATOR'">Delete</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="manifestation in manifestations">
                <td width="15%">
                  <img :src="manifestation.posterImagePath" width="100%" alt="Picture" style="display:block;"/>
                </td>
                <td>{{manifestation.name}}</td>
                <td>{{manifestation.manifestationType}}</td>
                <td>{{manifestation.dateTime}}</td>
                <td>{{manifestation.regularTicketPrice}}</td>
                <td>{{manifestation.location}}</td>
                <td>{{manifestation.averageGrade}}</td>
                <td>
                  <router-link :to="'manifestation/' + manifestation.id">View</router-link>
                </td>
                <td>
                  <a href="#" v-if="role === 'ADMINISTRATOR'" v-on:click="deleteManifestation(manifestation.id)">Delete</a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div v-else class="row justify-content-center">
        <h4>There is no manifestations</h4>
      </div>
      <div class="form-inline row justify-content-center" style="margin-top:20px; margin-bottom:80px;">
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="concert" v-on:click="filter" checked>
          <label class="form-check-label">Concert</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="festival" v-on:click="filter" checked>
          <label class="form-check-label">Festival</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="show" v-on:click="filter" checked>
          <label class="form-check-label">Show</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="opera" v-on:click="filter" checked>
          <label class="form-check-label">Opera</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="sport" v-on:click="filter" checked>
          <label class="form-check-label">Sport</label>
        </div>
        <div class="form-check form-check-inline">
          <input class="form-check-input" type="checkbox" id="soldOut" v-on:click="filter">
          <label class="form-check-label">Sold out</label>
        </div>
      </div>
    </div>
  </div>
  `
  ,

  mounted() {
    this.role = localStorage.getItem('role');

    axios.get("/api/manifestations/manifestations").then(response => this.loadManifestations(response.data));
  },

  methods: {
    loadManifestations(manifestationData) {
      var manifestations = [];
      manifestationData.forEach((manifestation) => {
        var grade;
        if (manifestation.averageGrade == 0) {
          grade = null;
        } else {
          grade = parseFloat(manifestation.averageGrade).toFixed(2);
        }

        manifestations.push({
          id: manifestation.id,
          name: manifestation.name,
          manifestationType: manifestation.manifestationType.charAt(0) + manifestation.manifestationType.slice(1).toLowerCase(),
          numOfAvailableSeats: manifestation.numOfAvailableSeats,
          dateTime: manifestation.dateTime,
          regularTicketPrice: parseFloat(manifestation.regularTicketPrice).toFixed(2),
          posterImagePath: manifestation.posterImagePath,
          location: manifestation.street + ', ' + manifestation.city,
          averageGrade: grade
        })
      });

      this.manifestations = manifestations;
      this.allManifestations = manifestations;

      this.manifestations.sort((a, b) => {
        return Math.abs(Date.now() - this.stringToDateTime(a.dateTime)) - Math.abs(Date.now() - this.stringToDateTime(b.dateTime));
      });

      this.checkLength();
    },

    stringToDateTime(dtString) {
      dateParts = dtString.split(' ')[0].split('.');
      time = dtString.split(' ')[1];
      return new Date(dateParts[1] + '/' + dateParts[0] + '/' + dateParts[2] + ' ' + time);
    },

    search() {
      axios.get('/api/manifestations/search?name=' + this.searchName + '&dateFrom=' + this.dateFrom + '&dateTo=' + this.dateTo + '&location=' + this.searchLocation + '&priceFrom=' + this.priceFrom + '&priceTo=' + this.priceTo).then(response => {
        this.loadManifestations(response.data);

        document.getElementById('concert').checked = true;
        document.getElementById('festival').checked = true;
        document.getElementById('show').checked = true;
        document.getElementById('opera').checked = true;
        document.getElementById('sport').checked = true;
        document.getElementById('soldOut').checked = false;
      });
    },

    sort(sortProp) {
      if (sortProp === 'name') {
        if (this.sorted)
          this.manifestations.sort((m1, m2) => m1.name.toLowerCase() > m2.name.toLowerCase() ? -1 : 1);
        else
          this.manifestations.sort((m1, m2) => m1.name.toLowerCase() > m2.name.toLowerCase() ? 1 : -1);
      } else if (sortProp === 'dateTime') {
        if (this.sorted)
          this.manifestations.sort((m1, m2) => this.stringToDateTime(m1.dateTime) > this.stringToDateTime(m2.dateTime) ? -1 : 1);
        else
          this.manifestations.sort((m1, m2) => this.stringToDateTime(m1.dateTime) > this.stringToDateTime(m2.dateTime) ? 1 : -1);
      } else if (sortProp === 'price') {
        if (this.sorted)
          this.manifestations.sort((m1, m2) => m1.regularTicketPrice - m2.regularTicketPrice);
        else
          this.manifestations.sort((m1, m2) => m2.regularTicketPrice - m1.regularTicketPrice);
      } else {
        if (this.sorted)
          this.manifestations.sort((m1, m2) => m1.location.toLowerCase() > m2.location.toLowerCase() ? -1 : 1);
        else
          this.manifestations.sort((m1, m2) => m1.location.toLowerCase() > m2.location.toLowerCase() ? 1 : -1);
      }

      this.sorted = !this.sorted;
    },

    filter() {
      var filteredType = [];

      if (document.getElementById('concert').checked)
        filteredType = filteredType.concat(this.allManifestations.filter(manifestation => manifestation.manifestationType === 'Concert'));
      if (document.getElementById('festival').checked)
        filteredType = filteredType.concat(this.allManifestations.filter(manifestation => manifestation.manifestationType === 'Festival'));
      if (document.getElementById('show').checked)
        filteredType = filteredType.concat(this.allManifestations.filter(manifestation => manifestation.manifestationType === 'Show'));
      if (document.getElementById('opera').checked)
        filteredType = filteredType.concat(this.allManifestations.filter(manifestation => manifestation.manifestationType === 'Opera'));
      if (document.getElementById('sport').checked)
        filteredType = filteredType.concat(this.allManifestations.filter(manifestation => manifestation.manifestationType === 'Sport'));

      var filteredSoldOut = [];
      if (!document.getElementById('soldOut').checked)
        filteredSoldOut = filteredSoldOut.concat(this.allManifestations.filter(manifestation => manifestation.numOfAvailableSeats !== 0));

      this.manifestations = filteredType.filter(value => filteredSoldOut.includes(value));
      this.checkLength();
    },

    checkLength() {
      if (this.manifestations.length === 0) {
        this.hasManifestations = false;
      } else {
        this.hasManifestations = true;
      }
    },

    deleteManifestation(manifestationId) {
      axios.get('/api/manifestations/deleteManifestation?id=' + manifestationId).then(response => this.loadManifestations(response.data));
    }
  }
});