Vue.component('locations', {
  data:
    function () {
      return {
        locations: [],
        hasLocations: false
      }
    },

  template: `
  <my_layout>
    <body class="text-center center">
      <div class="table-responsive" v-if="hasLocations">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Address</th>
              <th>Longitude</th>
              <th>Latitude</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="location in locations">
              <td>{{location.address}}</td>
              <td>{{location.longitude}}</td>
              <td>{{location.latitude}}</td>
              <td>
                <a href="#" v-on:click="deleteLocation(location.id)">Delete</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div v-else>
        <h4>There is no locations</h4>
      </div>
    </body>
    </my_layout>
    `,

  methods: {
    checkLength() {
      if (this.tickets.length === 0) {
        this.hasTickets = false;
      } else {
        this.hasTickets = true;
      }
    },

    deleteLocation(locationId) {
      axios.get('/api/locations/deleteLocation?id=' + locationId).then(response => {
        var locations = [];
        
        response.data.forEach((location) => {
          locations.push({
            id: location.id,
            address: location.street + ', ' + location.city + ', ' + location.postalNumber,
            longitude: parseFloat(location.longitude).toFixed(2),
            latitude: parseFloat(location.latitude).toFixed(2)
          })
        });
  
        this.locations = locations;
  
        if (this.locations.length === 0) {
          this.hasLocations = false;
        } else {
          this.hasLocations = true;
        }
      });
    }
  },

  mounted() {
    axios.get('/api/locations/locations').then(response => {
      var locations = [];
      
      response.data.forEach((location) => {
        locations.push({
          id: location.id,
          address: location.street + ', ' + location.city + ', ' + location.postalNumber,
          longitude: parseFloat(location.longitude).toFixed(2),
          latitude: parseFloat(location.latitude).toFixed(2)
        })
      });

      this.locations = locations;

      if (this.locations.length === 0) {
        this.hasLocations = false;
      } else {
        this.hasLocations = true;
      }
    });
  }
})