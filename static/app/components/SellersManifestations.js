Vue.component('sellersManifestations', {
  data:
    function () {
      return {
        manifestations: [],
        allowed: false,
        hasManifestations: false
      }
    },

  template: `
  <my_layout>
    <body class="text-center center">
      <div v-if="allowed">
        <div class="table-responsive" v-if="hasManifestations">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Type</th>
                <th>Available seats</th>
                <th>Date and time</th>
                <th>Regular ticket price</th>
                <th>Edit</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="manifestation in manifestations">
                <td>{{manifestation.id}}</td>
                <td>{{manifestation.name}}</td>
                <td>{{manifestation.manifestationType}}</td>
                <td>{{manifestation.numOfAvailableSeats}}</td>
                <td>{{manifestation.dateTime}}</td>
                <td>{{manifestation.regularTicketPrice}}</td>
                <td>
                  <router-link :to="'editManifestation/' + manifestation.id">Edit</router-link>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div v-else class="row justify-content-center">
          <h4>There is no manifestations</h4>
        </div>
      </div>
      <div v-else>
        <h4 id="errorMessage"></h4>
      </div>
    
    </body>
    </my_layout>
    `,

  methods: {
  },

  mounted() {
    axios.get('api/manifestations/sellersManifestations').then(response => {
      this.allowed = true;
      
      var manifestations = [];
      response.data.forEach((manifestation) => {
        var grade;
        if (manifestation.averageGrade == 0) {
          grade = null;
        } else {
          grade = parseFloat(manifestation.averageGrade).toFixed(2);
        }

        manifestations.push({
          id: manifestation.id,
          name: manifestation.name,
          manifestationType: manifestation.manifestationType.charAt(0) + manifestation.manifestationType.slice(1).toLowerCase(),
          numOfAvailableSeats: manifestation.numOfAvailableSeats,
          dateTime: manifestation.dateTime,
          regularTicketPrice: parseFloat(manifestation.regularTicketPrice).toFixed(2),
          posterImagePath: manifestation.posterImagePath,
          location: manifestation.street + ', ' + manifestation.city,
          averageGrade: grade
        })
      });

      this.manifestations = manifestations;
      
      if (this.manifestations.length === 0) {
        this.hasManifestations = false;
      } else {
        this.hasManifestations = true;
      }
    }).catch(function (error) {
      this.allowed = false;
      document.getElementById('errorMessage').innerHTML = error.response.data;
    });
  }
})