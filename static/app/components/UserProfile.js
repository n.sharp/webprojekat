Vue.component('userProfile', {
  data:
    function () {
      return {
        user: {}
      }
    },

  template: `
  <my_layout>
      <body>
        <div class="container-fluid">
          <div class="row" style="height: 100vh;">
            <div class="col">
            </div>
  
            <div class="col-4">
              <div>
                <div class="login-form" id="profileForm"></div>
                  <form>
                    <h2 class="text-center">My profile</h2>
                    <div class="form-group row">
                      <div class="col-6">
                        <label>Username</label>
                        <input v-model="user.username" type="text" class="form-control" readonly>
                      </div>
                      <div class="col-6">
                        <label>Password</label>
                        <input v-model="user.password" type="password" class="form-control" placeholder="Password">
                      </div>
                    </div>
                    <div class="form-group row">
                      <div class="col-6">
                        <label>First name</label>
                        <input v-model="user.firstName" type="text" class="form-control" placeholder="First Name">
                      </div>
                      <div class="col-6">
                        <label>Last name</label>
                        <input v-model="user.lastName" type="text" class="form-control" placeholder="Last Name">
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Date of birth</label>
                      <input v-model="user.dateOfBirth" class="form-control" type="text" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date of birth">
                    </div>
                    <div class="form-group">
                      <label>Date of birth</label>
                      <select class="form-control" id="gender" v-model="user.gender">
                        <option value="" disabled selected>Gender</option>
                        <option value="MALE">Male</option>
                        <option value="FEMALE">Female</option>
                      </select>
                    </div>
                    <div class="form-group" v-if="user.role === 'ADMINISTRATOR'">
                      <label>Moto</label>
                      <input v-model="user.moto" class="form-control" type="text" placeholder="Moto">
                    </div>
                    <div class="form-group row" v-if="user.role === 'BUYER'">
                      <div class="col-6">
                        <label>Points</label>
                        <input v-model="user.points" type="text" class="form-control" readonly>
                      </div>
                      <div class="col-6">
                        <label>Type</label>
                        <input v-model="user.buyerType" type="text" class="form-control" readonly>
                      </div>
                    </div>
                    <div class="form-group">
                      <button v-on:click="edit" class="btn btn-primary btn-block" type="submit">Save</button>
                    </div>
                    <div class="clearfix">
                      <a href="#" v-on:click="cancel" class="float-right">Cancel</a>
                    </div>
                  </form>
                </div>
              </div>
            <div class="col">
            </div>
          </div>
        </div>
        <div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="modalTitle">Error message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p id="modalMessage"></p>
              </div>
              <div class="modal-footer">
                <button type="reset" class="btn btn-primary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </body>
    </my_layout>
    `,

  methods: {
    edit() {
      if (this.user.password == '' || this.user.firstName == '' || this.user.lastName == '' || this.user.dateOfBirth == '' || this.user.gender == '') {
        document.getElementById('modalMessage').innerHTML = 'All of the fields must be filled.';
      } else {
        axios.post('/api/users/editUser', this.user).then(response => {
          document.getElementById('modalTitle').innerHTML = 'Successful';
          document.getElementById('modalMessage').innerHTML = 'You edited your data successfully!';
        }).catch(function (error) {
          document.getElementById('modalMessage').innerHTML = String(error.response.data);
        });
      }

      $('#userModal').modal('show');
    },

    cancel() {
      axios.get('/api/users/userProfile').then(response => {
        this.user = response.data;
        var dateFormatted = this.user.dateOfBirth.year.toString() + '-' + this.user.dateOfBirth.month.toString().padStart(2, '0') + '-' + this.user.dateOfBirth.day.toString().padStart(2, '0');
        this.user.dateOfBirth = dateFormatted;
      });
    }
  },

  mounted() {
    axios.get('/api/users/userProfile').then(response => {
      this.user = response.data;
      var dateFormatted = this.user.dateOfBirth.year.toString() + '-' + this.user.dateOfBirth.month.toString().padStart(2, '0') + '-' + this.user.dateOfBirth.day.toString().padStart(2, '0');
      this.user.dateOfBirth = dateFormatted;
    });
  }
})