Vue.component("approveComments", {
  data: function () {
    return {
      comments: [],
      hasComments: false
    }
  },

  template: `
  <my_layout>
  <div class="text-center mx-5 row" v-if="hasComments">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Author</th>
            <th>Manifestation id</th>
            <th>Text</th>
            <th>Grade</th>
            <th>Approve</th>
            <th>Reject</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="comment in comments">
            <td>{{comment.author}}</td>
            <td>{{comment.manifestation}}</td>
            <td>{{comment.text}}</td>
            <td>{{comment.grade}}</td>
            <td>
              <a href="#" v-on:click="approveReject(comment, true)">Approve</a>
            </td>
            <td>
              <a href="#" v-on:click="approveReject(comment, false)">Reject</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div v-else class="row justify-content-center">
    <h4>There are no comments to be approved</h4>
  </div>
</my_layout>
  `,

  mounted() {
    axios.get("/api/comments/getCreated").then(response => {
      this.comments = response.data;
      if (this.comments.length === 0) {
        this.hasComments = false;
      } else {
        this.hasComments = true;
      }
    });
  },

  methods: {
    approveReject(comment, approve) {
      axios.post("/api/comments/approveReject?approve=" + approve, comment).then(response => {
       
        this.comments = response.data;
        if (this.comments.length === 0) {
          this.hasComments = false;
        } else {
          this.hasComments = true;
        }
      });
    }
  }
})